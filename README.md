## Python Wikipedia Search Engine aka wikisearcher ##

Project made by:
  • Filippo Rinaldi, matr. 123889
  • Gabriele Mattioli, matr. 121928

Requirements: 
 - Python 3
 - Pipenv recommended

 We discourage using python 3.9 due to incompatibility problems with some used python 
 packages.
 Because we used Python3, we run only "pip" and "python", but if you have both Python2 and Python3
 installed you should use "pip3" and "python3" instead of "pip" and "python", because the last two
 would be for Python2.

---Initial configuration---

1) Install pipenv by running the following command:
    $ pip install pipenv

2) Go to the project base folder

3) Run the follwing command to install all the required packages
    $ pipenv install

4) Launch the newly created environment
    $ pipenv shell

5) Configure the "base.py" script in 'Normal' package in 'Code' folder in order to set the correct project environment and the other variabiles.


There are two different configuration approaches:

    1 : Normal mode -> consists of creating a small index, built with the best algorithm we have found. This mode was created to avoid
    long indexing times.
    After the creation of the index it will be possible to test the goodness of the index by searching for queries through a simple
    graphical interface.
    In normal mode you can directly use our gui to search for queries because there is an index, built with the best algorithm we have 
    found and based on the first 10 dumps taken from the site https://dumps.wikimedia.org/enwiki/, already present in the 'Index' 
    directory.    
    However if you want to try the 'Indexer' script and/or change the default alghoritm  
    two xml dump files are already present in the 'Xml' directory and they will automatically be used .

    Required scripts : all python files contained in the 'Normal' package in 'Code' folder/package but not in "Test" 

    2 : Test mode -> consists of creating a personalized index, based on several documents and therefore of larger dimensions. This mode 
    was used by us to find the algorithm with the best performance.A internet connection is required because there are two scripts that 
    have to download the Wikipedia dumps and the google relevant articles. Since the quantity of documents to be indexed is very high, 
    the process will be long and computationally complex for your computer, for this reason, if you want, you can skip the indexing 
    phase directly using the 'foo' index provided by us. After the indexing process there is a script to evaluate the 
    index goodness using google results as ground truth of relevant documents.
    You can also use the graphical interface to search for words not included in the evaluation script.

    Required scripts : all python files contained in the 'Code/Test' folder and 'Searcher_GUI.py', 'base.py' from 'Normal' folder

--NORMAL MODE--

Steps :
0) Configure base.py

1) Run 'Indexer.py' in Normal package to parse and create the index (optional)

2) Run 'Searcher_GUI.py' to search for your queries through GUI


0)  
    Note that the configuration file can be used as it is found; you can modify it in order
    to try different settings, but it is not essential. 
    In particular you can try to set the project absolute path and tune variables and functions in order to have
    the expected project behaviour, in particular you need to set "project_path" variabile manually if there are running problems and
    "index_creation_path" variable if you want to change the name of the index folder.
   
1)
    Inside the project there are already 2 files (2 XML multristream compressed).
    Running 'Indexer.py' will use these 2 XML files to create the index (it will take around xxx minutes).
    This script creates an index with the best algorithm we have found and saves it in the "index_creation_path" absolute path.
    Set "num_dumps_to_index" variable in base.py to set the number of XML files to use for indexing;
    the default is 2 (because you will already have 2 XML files inside the project).
    If you want to download more/different files to try different indexes, run the "DownloadDumps.py" (see Test Section).

2)
    With this script is possible to search different queries on our index, using a graphical interface.
    The GUI will display ten first 10 retrieved documents and some of their text sections containing the words that matched the query 
    highlighted in red.
    After a search, the system can suggest a possible query correction next to the '<- Apply Correction' button. Press the button to 
    correct the query and then press the 'Search' button to search for it.


    

--TEST MODE--

0) Configure base.py

1) Run (optional) 'DownloadGoogleResults.py' in Normal/Test folder to search and download the first 30 titles 
      of each query in google-queries variable (base.py) used as relevant documents in evaluation script .

2) Run (optional) 'DownloadDumps.py' to download a number of dumps depending on the 'num_dumps_to_download' (base.py) variable which   
      will be indexed later. The more you download, the more will be the time for downloading, parsing,
      building of the index and search for queries.

3) Run 'IndexerOnlyTitles.py' to parse and create the index used for tests.

4) Run 'Evaluation.py' to verify the quality of your index, it will be created a new folder in 'Evaluations_Exam' directory with inside the  
   'mean average' and 'normalized discounted         
   cumulative gain' values ​​and two qualitative graphs.

5) An alternative to (3) and (4) is to run 'AutomaticAnalyzer' (5) which is a script that automatically first creates a combination of      
   tokenizers and filters, then indexes the documents using the created combination and evaluates it with the 'Evaluation.py' script. 
   In the 'Evaluations_Exam' directory for each combination will be created a folder with inside the 'mean average' and 'normalized    
   discounted cumulative gain' values ​​and two qualitative graphs.
   Two summary graphs of all combinations will also be created.

0)  
    Note that the configuration file can be used as it is found; you can modify it in order
    to try different settings, but it is not essential. 
    In particular you can try to set the project absolute path and tune variables and functions in order to have
    the expected project behaviour, in particular you need to set "project_path" variabile manually if there are running problems and
    "index_creation_path" variable if you want to change the name of the index folder.

3) 
    Running 'IndexerOnlyTitles.py' will use the XML files downloaded with 'DownloadDumps.py' script or already present in the xml  
    folder to create the index (the more files to be indexed the more will be the time for parsing and building of the index ).
    This script creates an index with the combination of your choice that can be configured with the variables 'filters' and 
    'tokenizers' (base.py) and saves it in the "index_creation_path" absolute path.
    Unlike the 'Indexer' script this parsing script selects and adds to the index only the relevant documents for our test set of google 
    and optionally a full dump (to be selected in 'base.py'). In this way an index of reduced dimensions is created in a short time but 
    which still allows us to obtain a reliable evaluation
    Remember to change : 
    - "num_documents_to_index" variable in base.py to set the number of XML files to use for indexing if you have 
        download more than two dumps .
    - "dirty_index" variable to False if you don't want to add an entire dump to the index
    - "dirty_dump" variable to the filename of the xml file you want to add entirely to the index

4) 
    Set the following variables in base.py:
    - num_documents_to_evaluate to set the number of relevants documents that the evaluation will consider 
    - evaluation-path to set the directory to put the 'evaluation-folder'
    - evaluation-folder to set the directory to put the evaluation summary files
    This script will measure, with the help of two charts, the MAP and the NDCG of the current index
    based on a test of 30 queries. The tests use Google Search Engine as a ground thruth.

5) 
    The AutomaticAnalyzer is a script designed to accelerate indexes creation and evaluation.We used it to start finding the best combination of filters and tokenizers based on the indexing of 30 dumps. It is very useful to check many index 
    effectiveness, without starting indexing and evaluation for each of them.
    It combines a list of Tokenizers and Filters to generate different analyzers. 
    It always starts from the base analyzer set, composed of "a tokenizer" | LowerCaseFilter() | StopFilter() and then one or more filters, generating all possible permutations.
    For every analyzer generated, it creates the index and then evaluate it, computing MAP and NDCG. 
    To set the list of Tokenizers and Filters desired, let's see the lists "tokenizers" and "filters" in base.py. 
    At the end, it creates a csv with map, ndcg and size of every index called "gestione.csv" to quickly check the results. It also allow to create a chart comprehensive of all analyzers data, very useful to visualize different behaviors.
