Retrieved documents:
['a-dna', 'z-dna', 'dna', 'dna (disambiguation)', 'dna synthesis', 'nuclear dna', 'genomic dna', 'category:dna', 'nucleic acid double helix', 'nucleic acid structure']
Relevant documents:
['dna', 'dna (disambiguation)', 'nucleic acid double helix', 'a-dna', 'dna synthesis', 'z-dna', 'nucleic acid structure', 'genomic dna', 'category:dna', 'nuclear dna']
Final Recall level: 10/10
Average precision: 1.0
Normalized DCG for "DNA": 0.7842897701897656
***********************************************************************************************
Retrieved documents:
['apple inc.', 'apple ii', 'apple (symbolism)', 'apple', 'apple ii series', 'history of apple inc.', 'criticism of apple inc.', 'list of apple codenames', 'list of mergers and acquisitions by apple', 'apple store']
Relevant documents:
['apple inc.', 'apple', 'apple ii series', 'history of apple inc.', 'apple ii', 'criticism of apple inc.', 'apple (symbolism)', 'list of mergers and acquisitions by apple', 'list of apple codenames', 'apple store']
Final Recall level: 10/10
Average precision: 1.0
Normalized DCG for "Apple": 0.8715590155148106
***********************************************************************************************
Retrieved documents:
['category:epigenetics', 'computational epigenetics', 'epigenetics', 'helmholtz zentrum münchen', 'template:gene expression', 'epigenomics', 'nuclear dna', 'ralf reski', 'dna']
Relevant documents:
['epigenetics', 'transgenerational epigenetic inheritance', 'behavioral epigenetics', 'computational epigenetics', 'epigenetic therapy', 'epigenetics in learning and memory', 'epigenetic theories of homosexuality', 'category:epigenetics', 'epigenetics of schizophrenia', 'epigenomics']
Final Recall level: 4/10
Average precision: 0.36666666666666664
Normalized DCG for "Epigenetics": 0.46492489316044294
***********************************************************************************************
Retrieved documents:
['hollywood (disambiguation)', 'hollywood, florida', 'hollywood sign', 'hollywood', 'hollywood, pennsylvania', 'classical hollywood cinema', 'hollywood (british tv series)', 'list of hollywood-inspired nicknames', 'hollywood, abington township, pennsylvania', 'portal:california/selected picture/2']
Relevant documents:
['hollywood', 'hollywood (miniseries)', 'cinema of the united states', 'hollywood, florida', 'hollywood (disambiguation)', 'hollywood sign', 'classical hollywood cinema', 'hollywood (british tv series)', 'hollywood (programming language)', 'list of hollywood-inspired nicknames']
Final Recall level: 7/10
Average precision: 0.6565476190476189
Normalized DCG for "Hollywood": 0.5522439975229527
***********************************************************************************************
Retrieved documents:
['maya', 'maya peoples', 'autodesk maya', 'maya architecture', 'maya script', 'maya calendar', 'maya religion', 'maya (religion)', 'classic maya collapse', 'ancient maya art']
Relevant documents:
['maya civilization', 'maya peoples', 'maya (religion)', 'maya', 'autodesk maya', 'maya calendar', 'mayan languages', 'maya script', 'maya architecture', 'maya hawke']
Final Recall level: 7/10
Average precision: 0.6875
Normalized DCG for "Maya": 0.677707878602652
***********************************************************************************************
Retrieved documents:
['microsoft', 'microsoft home', 'history of microsoft', 'criticism of microsoft', 'list of microsoft software', 'microsoft windows', 'list of microsoft 365 applications', 'microsoft live', 'list of mergers and acquisitions by microsoft', 'united states v. microsoft corp.']
Relevant documents:
['microsoft', 'history of microsoft', 'list of mergers and acquisitions by microsoft', 'criticism of microsoft', 'microsoft home', 'united states v. microsoft corp.', 'microsoft 365', 'microsoft windows', 'list of microsoft software', 'microsoft live']
Final Recall level: 9/10
Average precision: 0.866388888888889
Normalized DCG for "Microsoft": 0.8742994454588918
***********************************************************************************************
Retrieved documents:
['precision', 'objective precision', 'precision engineering', 'precision (statistics)', 'precision and recall', 'accuracy and precision', 'precision (computer science)', 'category:precision sports', 'mean average precision', 'f-score']
Relevant documents:
['precision and recall', 'precision', 'accuracy and precision', 'precision (computer science)', 'precision (statistics)', 'f-score', 'precision', 'confusion matrix', 'precision engineering', 'evaluation measures (information retrieval)']
Final Recall level: 7/10
Average precision: 0.5607142857142857
Normalized DCG for "Precision": 0.6384353284935083
***********************************************************************************************
Retrieved documents:
['tuscany', 'march of tuscany', 'category:tuscany', 'category:provinces of tuscany', 'grand duchy of tuscany', 'category:cities and towns in tuscany', 'san gimignano', 'montepulciano', 'villa garzoni', 'lucca']
Relevant documents:
['tuscany', 'grand duchy of tuscany', 'tuscan wine', 'march of tuscany', 'category:provinces of tuscany', 'category:tuscany', 'san gimignano', 'montepulciano', 'category:cities and towns in tuscany', 'lucca']
Final Recall level: 9/10
Average precision: 0.89
Normalized DCG for "Tuscany": 0.8056529654728473
***********************************************************************************************
Retrieved documents:
['99 luftballons', '99 luftballons (album)', '99', 'red balloon', 'carlo karges', 'jörn-uwe fahrenkrog-petersen', 'goldfinger (band)', 'nena', 'list of speed racer episodes', 'solar energy']
Relevant documents:
['99 luftballons', '99 luftballons (album)', 'nena', 'talk:99 luftballons', 'nena (band)', 'jörn-uwe fahrenkrog-petersen', 'red balloon', 'carlo karges', 'goldfinger (band)', '99']
Final Recall level: 8/10
Average precision: 0.8
Normalized DCG for "99 balloons": 0.8327473360986867
***********************************************************************************************
Retrieved documents:
['computer programming', 'outline of computer programming', 'category:computer programming', 'programming language', 'computer program', 'c (programming language)', 'list of programming languages', 'programmer', 'things a computer scientist rarely talks about', 'translator (computing)']
Relevant documents:
['computer programming', 'computer program', 'category:computer programming', 'programmer', 'computer programming', 'programming language', 'portal:computer programming', 'outline of computer programming', 'list of programming languages', 'c (programming language)']
Final Recall level: 8/10
Average precision: 0.8
Normalized DCG for "Computer Programming": 0.7212269977135218
***********************************************************************************************
Retrieved documents:
['financial crisis', '1997 asian financial crisis', 'global financial crisis in september 2008', '2008–2011 icelandic financial crisis', 'subprime mortgage crisis', 'great recession', 'panic of 1907', 'microsoft']
Relevant documents:
['financial crisis', 'financial crisis of 2007–2008', 'great recession', 'list of economic crises', '2008–2011 icelandic financial crisis', 'global financial crisis in september 2008', 'global economic crisis', 'subprime mortgage crisis', 'economic collapse', 'panic of 1907']
Final Recall level: 6/10
Average precision: 0.49071428571428566
Normalized DCG for "Financial meltdown": 0.5669158730758482
***********************************************************************************************
Retrieved documents:
['justin timberlake', 'justin timberlake discography', 'cry me a river (justin timberlake song)', 'list of awards and nominations received by justin timberlake', 'justified (album)', 'futuresex/lovesounds', 'shelby forest, tennessee', 'quentin harris', 'kiis-fm jingle ball', "it's the end of the world"]
Relevant documents:
['justin timberlake', 'justin timberlake discography', 'justin timberlake videography', 'cry me a river (justin timberlake song)', 'justified (album)', 'mirrors (justin timberlake song)', 'list of songs recorded by justin timberlake', 'man of the woods', 'futuresex/lovesounds', 'list of awards and nominations received by justin timberlake']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Justin Timberlake": 0.8329177509089729
***********************************************************************************************
Retrieved documents:
['least squares', 'linear least squares', 'total least squares', 'weighted least squares', 'generalized least squares', 'indirect least squares', 'non-linear least squares', 'partial least squares regression', 'ordinary least squares', 'simple linear regression']
Relevant documents:
['least squares', 'ordinary least squares', 'linear least squares', 'total least squares', 'simple linear regression', 'non-linear least squares', 'generalized least squares', 'weighted least squares', 'linear regression', 'partial least squares regression']
Final Recall level: 9/10
Average precision: 0.8521031746031745
Normalized DCG for "Least Squares": 0.8927310347381691
***********************************************************************************************
Retrieved documents:
['exploration of mars', 'mars exploration rover', 'opportunity (rover)', 'rover (space exploration)', 'wikipedia:six degrees of wikipedia', '2009 cannes film festival']
Relevant documents:
['mars rover', 'mars exploration rover', 'list of artificial objects on mars', 'curiosity (rover)', 'exploration of mars', 'mars 2020', 'rover (space exploration)', 'opportunity (rover)', 'comparison of embedded computer systems on board the mars rovers', 'mars landing']
Final Recall level: 4/10
Average precision: 0.4
Normalized DCG for "Mars robots": 0.4625638925488031
***********************************************************************************************
Retrieved documents:
['page 6', 'will page', 'wikipedia:articles for deletion/variations on the six degrees of kevin bacon game', 'wikipedia:articles for deletion/olympus chrome six', 'wikipedia:six degrees of wikipedia', 'north boone community unit school district 200', 'claudia cohen', 'wikipedia:redirects for discussion/log/2008 may 18', 'the confusions', 'new york post']
Relevant documents:
['new york post', 'page 6', 'richard johnson (columnist)', 'claudia cohen', 'jared paul stern', 'dailyfill', 'paula froelich', 'sean delonas', 'james brady (columnist)', 'sixdegrees.com']
Final Recall level: 3/10
Average precision: 0.15857142857142856
Normalized DCG for "Page six": 0.4479929062997194
***********************************************************************************************
Retrieved documents:
['roman empire', 'roman empire (disambiguation)', 'western roman empire', 'history of the roman empire', 'fall of the western roman empire', 'roman emperor', 'list of roman emperors', 'wikipedia:wikiproject military history/peer review/late roman army', 'ancient rome', 'march of tuscany']
Relevant documents:
['roman empire', 'history of the roman empire', 'roman emperor', 'list of roman emperors', 'demography of the roman empire', 'fall of the western roman empire', 'western roman empire', 'roman empire (disambiguation)', 'roman empire (tv series)', 'ancient rome']
Final Recall level: 8/10
Average precision: 0.788888888888889
Normalized DCG for "Roman Empire": 0.7676148839200567
***********************************************************************************************
Retrieved documents:
['solar energy', 'solar 1', 'renewable energy', 'solar panel', 'solar power by country', 'solar power in the united states', 'photovoltaics', 'feed-in tariffs in australia', 'unsw school of photovoltaic and renewable energy engineering', 'maccready gossamer penguin']
Relevant documents:
['solar energy', 'solar power', 'solar panel', 'outline of solar energy', 'solar power in italy', 'solar power by country', 'concentrated solar power', 'solar power in the united states', 'photovoltaics', 'renewable energy']
Final Recall level: 6/10
Average precision: 0.49071428571428566
Normalized DCG for "Solar energy": 0.5577817972966178
***********************************************************************************************
Retrieved documents:
['statistical significance', 'statistical hypothesis testing', 'exclusion of the null hypothesis', 'data dredging', 'p-value', 'power of a test', 'elston–stewart algorithm', 'binomial test', 'multiple comparisons problem', 'type i and type ii errors']
Relevant documents:
['statistical significance', 'p-value', 'statistical hypothesis testing', 'data dredging', 'power of a test', 'multiple comparisons problem', 'type i and type ii errors', 'exclusion of the null hypothesis', 'binomial test', 'talk:statistical significance']
Final Recall level: 9/10
Average precision: 0.866388888888889
Normalized DCG for "Statistical Significance": 0.9106771242600812
***********************************************************************************************
Retrieved documents:
['steve jobs', 'laurene powell jobs', 'steve wozniak', 'next', 'history of apple inc.', 'apple ii', 'apple inc.', 'wikipedia:miscellany for deletion/user:sharkface217/award center 2', 'criticism of apple inc.', 'wikipedia:bot requests/archive 20']
Relevant documents:
['steve jobs', 'steve jobs (film)', 'steve jobs (book)', 'steve jobs', 'steve wozniak', 'jobs (film)', 'next', 'laurene powell jobs', 'steve jobs (disambiguation)', 'list of artistic depictions of steve jobs']
Final Recall level: 4/10
Average precision: 0.4
Normalized DCG for "Steve Jobs": 0.4592660512617044
***********************************************************************************************
Retrieved documents:
['maya', 'maya peoples', 'autodesk maya', 'maya architecture', 'maya script', 'maya calendar', 'maya religion', 'maya (religion)', 'classic maya collapse', 'ancient maya art']
Relevant documents:
['maya civilization', 'maya peoples', 'maya architecture', 'history of the maya civilization', 'maya city', 'chichen itza', 'maya', 'classic maya collapse', 'ancient maya art', 'maya religion']
Final Recall level: 6/10
Average precision: 0.4476984126984126
Normalized DCG for "The Maya": 0.5104518323413912
***********************************************************************************************
Retrieved documents:
['triple cross', 'the triple cross', 'triple cross (1966 film)', 'triple product', 'papal cross', 'three crosses square', 'pekin street historic district', 'deus, in adiutorium meum intende', 'foolish (shawty lo song)', 'armour transportation systems']
Relevant documents:
['triple cross', 'triple cross (1966 film)', 'the triple cross', 'papal cross', 'triple product', 'joe palooka in triple cross', 'three crosses square', 'eddie chapman', 'christian cross variants', 'triple crossed (film)']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Triple Cross": 0.9007604381599593
***********************************************************************************************
Retrieved documents:
['constitution of myanmar', 'constitution', 'constitution of the united states', 'assembly of experts for constitution', 'california constitution', 'history of the united states constitution', 'list of amendments to the united states constitution', 'article two of the united states constitution', 'article one of the united states constitution', '2008 constitution of the republic of the union of myanmar']
Relevant documents:
['constitution of the united states', 'article one of the united states constitution', 'list of amendments to the united states constitution', 'constitution', 'history of the united states constitution', 'united states bill of rights', 'article two of the united states constitution', 'first amendment to the united states constitution', 'fourteenth amendment to the united states constitution', 'second amendment to the united states constitution']
Final Recall level: 6/10
Average precision: 0.35297619047619044
Normalized DCG for "US Constitution": 0.6197972003982735
***********************************************************************************************
Retrieved documents:
['eye of horus', 'litany of the eye of horus', 'eye of horus (video game)', 'horus', 'eye of ra', 'eye of providence', 'set (deity)', 'osiris myth', 'wadjet']
Relevant documents:
['eye of horus', 'horus', 'eye of ra', 'talk:eye of horus', 'litany of the eye of horus', 'eye of horus (video game)', 'eye of providence', 'osiris myth', 'set (deity)', 'wadjet']
Final Recall level: 9/10
Average precision: 0.9
Normalized DCG for "Eye of Horus": 0.8104179280437744
***********************************************************************************************
Retrieved documents:
['young and rich', 'what do you want from live', 'palindrome']
Relevant documents:
['palindrome', 'madam adam', 'mark saltveit', 'the palindromist', 'young and rich', 'madam secretary (tv series)', 'anda adam', 'dax jordan', 'adam ant', 'what do you want from live']
Final Recall level: 3/10
Average precision: 0.3
Normalized DCG for "Madam I'm Adam": 0.3860276388058759
***********************************************************************************************
Retrieved documents:
['mean average precision', 'mean reciprocal rank', 'precision and recall', 'accuracy and precision', 'wikipedia:articles for deletion/introduction to genetics', 'wikipedia:reference desk/archives/science/2008 may 10', 'wikipedia:reference desk/archives/humanities/2008 may 7', 'statistical hypothesis testing', 'united states marine corps']
Relevant documents:
['mean average precision', 'evaluation measures (information retrieval)', 'precision and recall', 'mean absolute percentage error', 'mean reciprocal rank', 'f-score', 'accuracy and precision', 'information retrieval', 'average absolute deviation', 'symmetric mean absolute percentage error']
Final Recall level: 4/10
Average precision: 0.4
Normalized DCG for "Mean Average Precision": 0.6271330010411823
***********************************************************************************************
Retrieved documents:
['list of nobel laureates in physics', 'list of female nobel laureates', 'nobel prize', 'ig nobel prize', 'list of nobel laureates', 'list of nobel laureates by university affiliation', 'list of nobel laureates by country', 'nobel prize controversies', 'list of nobel laureates in chemistry', 'george c. schatz']
Relevant documents:
['list of nobel laureates in physics', 'nobel prize in physics', 'list of nobel laureates', 'nobel prize', 'list of nobel laureates by country', 'nobel prize controversies', 'list of nobel laureates in chemistry', 'ig nobel prize', 'list of italian nobel laureates', 'list of female nobel laureates']
Final Recall level: 8/10
Average precision: 0.7621031746031746
Normalized DCG for "Physics Nobel Prizes": 0.709792708750915
***********************************************************************************************
Retrieved documents:
['the manual', "owner's manual", 'rtfm', 'ddrescue', 'saf-te', 'user guide', 'wikipedia:articles for deletion/tank mania', 'wikipedia:wikiproject record labels', 'wikipedia:articles for deletion/grading living dead dolls', 'wikipedia:suspected sock puppets/fnagaton']
Relevant documents:
['rtfm', "owner's manual", "owner's manual (tv series)", 'user guide', 'autonomous spaceport drone ship', 'the manual', 'floss manuals', 'wikipedia:simplified manual of style', 'manual', 'file:read the hamster manual advertisement.jpg']
Final Recall level: 4/10
Average precision: 0.36666666666666664
Normalized DCG for "Read the manual": 0.6227194018695771
***********************************************************************************************
Retrieved documents:
['category:spanish civil war', 'spanish civil war', '1938–39 in the spanish civil war', 'foreign involvement in the spanish civil war', '1936 in the spanish civil war', 'background of the spanish civil war', 'rosario sánchez mora', 'spanish cruiser cristóbal colón', 'russia–spain relations', "confederate-union veterans' monument in morgantown"]
Relevant documents:
['spanish civil war', 'foreign involvement in the spanish civil war', 'republican faction (spanish civil war)', '1936 in the spanish civil war', 'background of the spanish civil war', 'nationalist faction (spanish civil war)', 'german involvement in the spanish civil war', '1938–39 in the spanish civil war', 'spanish coup of july 1936', 'category:spanish civil war']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Spanish Civil War": 0.6938612224000502
***********************************************************************************************
Retrieved documents:
['christmas is coming', 'mageina tovah', 'david slade', 'palindrome', 'roman empire']
Relevant documents:
['the palindromist', 'palindrome', 'david slade', 'hamsa (bird)', 'geb', 'talk:palindrome', 'greylag goose', 'mageina tovah', 'tengri', 'christmas is coming']
Final Recall level: 4/10
Average precision: 0.4
Normalized DCG for "Do geese see god": 0.3995753137567115
***********************************************************************************************
Retrieved documents:
['much ado about nothing', 'much ado about nothing (1993 film)', 'much ado', 'dogberry', 'ewan hooper', 'withers building', 'wikipedia:village pump (policy)/archive 46', 'wikipedia:requests for arbitration/c68-fm-sv/workshop', "wikipedia:administrators' noticeboard/archive144"]
Relevant documents:
['much ado about nothing', 'much ado about nothing (1993 film)', 'much ado about nothing (2012 film)', 'don pedro (much ado about nothing)', 'much ado about nothing (opera)', 'beatrice (much ado about nothing)', 'much ado about nothing (disambiguation)', 'much ado', 'template:much ado about nothing', 'much ado about nothing (1973 film)']
Final Recall level: 3/10
Average precision: 0.3
Normalized DCG for "Much ado about nothing": 0.661676868922715
***********************************************************************************************
