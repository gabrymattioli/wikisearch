Retrieved documents:
['dna', 'nucleic acid double helix', 'z-dna', 'epigenomics', 'epigenetics', 'dna synthesis', 'nuclear dna', 'nucleic acid structure', 'dna (disambiguation)', 'nucleic acid']
Relevant documents:
['dna', 'dna (disambiguation)', 'nucleic acid double helix', 'a-dna', 'dna synthesis', 'z-dna', 'nucleic acid structure', 'genomic dna', 'category:dna', 'nuclear dna']
Final Recall level: 7/10
Average precision: 0.5908730158730158
Normalized DCG for "DNA": 0.7777637459246887
***********************************************************************************************
Retrieved documents:
['apple inc.', 'criticism of apple inc.', 'history of apple inc.', 'list of mergers and acquisitions by apple', 'apple ii series', 'steve wozniak', 'list of apple codenames', 'apple ii', 'next', 'apple (symbolism)']
Relevant documents:
['apple inc.', 'apple', 'apple ii series', 'history of apple inc.', 'apple ii', 'criticism of apple inc.', 'apple (symbolism)', 'list of mergers and acquisitions by apple', 'list of apple codenames', 'apple store']
Final Recall level: 8/10
Average precision: 0.7532142857142856
Normalized DCG for "Apple": 0.7076702363937795
***********************************************************************************************
Retrieved documents:
['epigenetics', 'computational epigenetics', 'epigenomics', 'dna', 'category:epigenetics', 'nuclear dna', 'template:gene expression', 'helmholtz zentrum münchen', 'ralf reski']
Relevant documents:
['epigenetics', 'transgenerational epigenetic inheritance', 'behavioral epigenetics', 'computational epigenetics', 'epigenetic therapy', 'epigenetics in learning and memory', 'epigenetic theories of homosexuality', 'category:epigenetics', 'epigenetics of schizophrenia', 'epigenomics']
Final Recall level: 4/10
Average precision: 0.38
Normalized DCG for "Epigenetics": 0.572398965661184
***********************************************************************************************
Retrieved documents:
['classical hollywood cinema', 'hollywood sign', 'hollywood, florida', 'national register of historic places listings in los angeles', 'cinema of the united states', 'hollywood (disambiguation)', 'hollywood (british tv series)', 'list of awards and nominations received by justin timberlake', 'justin timberlake', 'jtg daugherty racing']
Relevant documents:
['hollywood', 'hollywood (miniseries)', 'cinema of the united states', 'hollywood, florida', 'hollywood (disambiguation)', 'hollywood sign', 'classical hollywood cinema', 'hollywood (british tv series)', 'hollywood (programming language)', 'list of hollywood-inspired nicknames']
Final Recall level: 6/10
Average precision: 0.549047619047619
Normalized DCG for "Hollywood": 0.383742377826315
***********************************************************************************************
Retrieved documents:
['maya religion', 'maya (religion)', 'maya peoples', 'maya script', 'ancient maya art', 'maya calendar', 'classic maya collapse', 'mayan languages', 'maya', 'yucatec maya language']
Relevant documents:
['maya civilization', 'maya peoples', 'maya (religion)', 'maya', 'autodesk maya', 'maya calendar', 'mayan languages', 'maya script', 'maya architecture', 'maya hawke']
Final Recall level: 6/10
Average precision: 0.38749999999999996
Normalized DCG for "Maya": 0.5302793591232513
***********************************************************************************************
Retrieved documents:
['microsoft', 'list of mergers and acquisitions by microsoft', 'history of microsoft', 'criticism of microsoft', 'microsoft windows', 'list of microsoft software', 'united states v. microsoft corp.', 'microsoft home', 'xbox (console)', 'list of microsoft 365 applications']
Relevant documents:
['microsoft', 'history of microsoft', 'list of mergers and acquisitions by microsoft', 'criticism of microsoft', 'microsoft home', 'united states v. microsoft corp.', 'microsoft 365', 'microsoft windows', 'list of microsoft software', 'microsoft live']
Final Recall level: 8/10
Average precision: 0.8
Normalized DCG for "Microsoft": 0.9383939704462901
***********************************************************************************************
Retrieved documents:
['wikipedia:requests for arbitration/c68-fm-sv/workshop', 'precision and recall', 'accuracy and precision', 'precision', 'f-score', 'precision engineering', 'objective precision', 'precision (statistics)', 'precision (computer science)', 'engineering fit']
Relevant documents:
['precision and recall', 'precision', 'accuracy and precision', 'precision (computer science)', 'precision (statistics)', 'f-score', 'precision', 'confusion matrix', 'precision engineering', 'evaluation measures (information retrieval)']
Final Recall level: 7/10
Average precision: 0.5077777777777779
Normalized DCG for "Precision": 0.7080176294485555
***********************************************************************************************
Retrieved documents:
['grand duchy of tuscany', 'tuscany', 'march of tuscany', 'lucca', 'san gimignano', '2007–08 eccellenza', 'montepulciano', 'category:provinces of tuscany', 'category:tuscany', 'villa garzoni']
Relevant documents:
['tuscany', 'grand duchy of tuscany', 'tuscan wine', 'march of tuscany', 'category:provinces of tuscany', 'category:tuscany', 'san gimignano', 'montepulciano', 'category:cities and towns in tuscany', 'lucca']
Final Recall level: 8/10
Average precision: 0.7621031746031746
Normalized DCG for "Tuscany": 0.86254629457721
***********************************************************************************************
Retrieved documents:
['99 luftballons', '99', '99 luftballons (album)', 'red balloon', 'nena', 'solar energy', 'subprime mortgage crisis', 'list of speed racer episodes', 'goldfinger (band)', 'carlo karges']
Relevant documents:
['99 luftballons', '99 luftballons (album)', 'nena', 'talk:99 luftballons', 'nena (band)', 'jörn-uwe fahrenkrog-petersen', 'red balloon', 'carlo karges', 'goldfinger (band)', '99']
Final Recall level: 7/10
Average precision: 0.6366666666666667
Normalized DCG for "99 balloons": 0.73921260648265
***********************************************************************************************
Retrieved documents:
['apple inc.', 'programming language', 'list of programming languages', 'c (programming language)', 'outline of computer programming', 'computer program', 'computer programming', 'history of apple inc.', 'programmer', 'apple ii series']
Relevant documents:
['computer programming', 'computer program', 'category:computer programming', 'programmer', 'computer programming', 'programming language', 'portal:computer programming', 'outline of computer programming', 'list of programming languages', 'c (programming language)']
Final Recall level: 7/10
Average precision: 0.5184920634920636
Normalized DCG for "Computer Programming": 0.39270706408716677
***********************************************************************************************
Retrieved documents:
['subprime mortgage crisis', 'financial crisis', '2008–2011 icelandic financial crisis', '1997 asian financial crisis', 'great recession', 'global financial crisis in september 2008', 'panic of 1907', 'microsoft', 'electricity pricing']
Relevant documents:
['financial crisis', 'financial crisis of 2007–2008', 'great recession', 'list of economic crises', '2008–2011 icelandic financial crisis', 'global financial crisis in september 2008', 'global economic crisis', 'subprime mortgage crisis', 'economic collapse', 'panic of 1907']
Final Recall level: 6/10
Average precision: 0.549047619047619
Normalized DCG for "Financial meltdown": 0.6102884255080335
***********************************************************************************************
Retrieved documents:
['justin timberlake', 'list of awards and nominations received by justin timberlake', 'futuresex/lovesounds', 'justin timberlake discography', 'justified (album)', 'cry me a river (justin timberlake song)', 'sticky & sweet tour', 'kiis-fm jingle ball', 'australian idol (season 2)', 'quentin harris']
Relevant documents:
['justin timberlake', 'justin timberlake discography', 'justin timberlake videography', 'cry me a river (justin timberlake song)', 'justified (album)', 'mirrors (justin timberlake song)', 'list of songs recorded by justin timberlake', 'man of the woods', 'futuresex/lovesounds', 'list of awards and nominations received by justin timberlake']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Justin Timberlake": 0.6913681145527952
***********************************************************************************************
Retrieved documents:
['least squares', 'linear least squares', 'ordinary least squares', 'total least squares', 'wikipedia:requests for arbitration/c68-fm-sv/workshop', 'wikipedia:articles for creation/2008-05-16', 'non-linear least squares', 'chi-squared test', 'partial least squares regression', 'list of nabari no ou characters']
Relevant documents:
['least squares', 'ordinary least squares', 'linear least squares', 'total least squares', 'simple linear regression', 'non-linear least squares', 'generalized least squares', 'weighted least squares', 'linear regression', 'partial least squares regression']
Final Recall level: 6/10
Average precision: 0.5380952380952382
Normalized DCG for "Least Squares": 0.871905530763268
***********************************************************************************************
Retrieved documents:
['exploration of mars', 'opportunity (rover)', 'mars exploration rover', 'mars landing', 'mars rover', 'rover (space exploration)', 'peter smith (scientist)', 'wikipedia:six degrees of wikipedia', 'list of mergers and acquisitions by microsoft', 'scandal (japanese band)']
Relevant documents:
['mars rover', 'mars exploration rover', 'list of artificial objects on mars', 'curiosity (rover)', 'exploration of mars', 'mars 2020', 'rover (space exploration)', 'opportunity (rover)', 'comparison of embedded computer systems on board the mars rovers', 'mars landing']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Mars robots": 0.5475925956436897
***********************************************************************************************
Retrieved documents:
['wikipedia:wikiproject spam/coireports/2008, may 11', "wikipedia:administrators' noticeboard/incidentarchive416", 'dna', "wikipedia:administrators' noticeboard/archive144", 'ancient rome', 'ss kroonland', 'epigenetics', 'history of apple inc.', 'wikipedia:templates for deletion/log/2008 may 13', 'woolly mammoth']
Relevant documents:
['new york post', 'page 6', 'richard johnson (columnist)', 'claudia cohen', 'jared paul stern', 'dailyfill', 'paula froelich', 'sean delonas', 'james brady (columnist)', 'sixdegrees.com']
Final Recall level: 0/10
Average precision: 0
Normalized DCG for "Page six": 0.0
***********************************************************************************************
Retrieved documents:
['roman empire', 'ancient rome', 'western roman empire', 'history of the roman empire', 'fall of the western roman empire', 'roman emperor', 'list of roman emperors', 'wikipedia:wikiproject spam/linkreports/fordham.edu', 'genghis khan', 'roman empire (disambiguation)']
Relevant documents:
['roman empire', 'history of the roman empire', 'roman emperor', 'list of roman emperors', 'demography of the roman empire', 'fall of the western roman empire', 'western roman empire', 'roman empire (disambiguation)', 'roman empire (tv series)', 'ancient rome']
Final Recall level: 8/10
Average precision: 0.78
Normalized DCG for "Roman Empire": 0.7667936918789839
***********************************************************************************************
Retrieved documents:
['solar energy', 'renewable energy', 'solar power in the united states', 'solar power by country', 'photovoltaics', 'solar panel', 'feed-in tariffs in australia', 'electricity pricing', 'solar 1', 'apple inc.']
Relevant documents:
['solar energy', 'solar power', 'solar panel', 'outline of solar energy', 'solar power in italy', 'solar power by country', 'concentrated solar power', 'solar power in the united states', 'photovoltaics', 'renewable energy']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Solar energy": 0.5750961630633049
***********************************************************************************************
Retrieved documents:
['statistical hypothesis testing', 'statistical significance', 'exclusion of the null hypothesis', 'p-value', 'power of a test', 'multiple comparisons problem', 'data dredging', 'type i and type ii errors', 'chi-squared test', 'ordinary least squares']
Relevant documents:
['statistical significance', 'p-value', 'statistical hypothesis testing', 'data dredging', 'power of a test', 'multiple comparisons problem', 'type i and type ii errors', 'exclusion of the null hypothesis', 'binomial test', 'talk:statistical significance']
Final Recall level: 8/10
Average precision: 0.8
Normalized DCG for "Statistical Significance": 0.897777118325595
***********************************************************************************************
Retrieved documents:
['apple inc.', 'steve wozniak', 'history of apple inc.', 'laurene powell jobs', 'next', 'wikipedia:bot requests/archive 20', 'criticism of apple inc.', 'wikipedia:miscellany for deletion/user:sharkface217/award center 2', 'subprime mortgage crisis', 'microsoft']
Relevant documents:
['steve jobs', 'steve jobs (film)', 'steve jobs (book)', 'steve jobs', 'steve wozniak', 'jobs (film)', 'next', 'laurene powell jobs', 'steve jobs (disambiguation)', 'list of artistic depictions of steve jobs']
Final Recall level: 3/10
Average precision: 0.16
Normalized DCG for "Steve Jobs": 0.15361582199010898
***********************************************************************************************
Retrieved documents:
['maya religion', 'maya (religion)', 'maya peoples', 'maya script', 'ancient maya art', 'maya calendar', 'classic maya collapse', 'mayan languages', 'maya', 'yucatec maya language']
Relevant documents:
['maya civilization', 'maya peoples', 'maya architecture', 'history of the maya civilization', 'maya city', 'chichen itza', 'maya', 'classic maya collapse', 'ancient maya art', 'maya religion']
Final Recall level: 5/10
Average precision: 0.3393650793650793
Normalized DCG for "The Maya": 0.2990675420222367
***********************************************************************************************
Retrieved documents:
['triple product', 'triple cross (1966 film)', 'three crosses square', 'triple cross', '2008 bigpond 400', 'papal cross', 'eddie chapman', 'ss kroonland', 'the triple cross', 'agriculture in benin']
Relevant documents:
['triple cross', 'triple cross (1966 film)', 'the triple cross', 'papal cross', 'triple product', 'joe palooka in triple cross', 'three crosses square', 'eddie chapman', 'christian cross variants', 'triple crossed (film)']
Final Recall level: 7/10
Average precision: 0.6468253968253967
Normalized DCG for "Triple Cross": 0.7628618234058885
***********************************************************************************************
Retrieved documents:
['constitution of the united states', 'history of the united states constitution', 'second amendment to the united states constitution', 'article one of the united states constitution', 'fourteenth amendment to the united states constitution', 'list of amendments to the united states constitution', 'first amendment to the united states constitution', 'constitution of myanmar', 'article two of the united states constitution', '2008 california proposition 8']
Relevant documents:
['constitution of the united states', 'article one of the united states constitution', 'list of amendments to the united states constitution', 'constitution', 'history of the united states constitution', 'united states bill of rights', 'article two of the united states constitution', 'first amendment to the united states constitution', 'fourteenth amendment to the united states constitution', 'second amendment to the united states constitution']
Final Recall level: 8/10
Average precision: 0.788888888888889
Normalized DCG for "US Constitution": 0.7839754638949703
***********************************************************************************************
Retrieved documents:
['horus', 'osiris myth', 'eye of ra', 'eye of providence', 'eye of horus', 'set (deity)', 'eye of horus (video game)', 'wadjet', 'litany of the eye of horus']
Relevant documents:
['eye of horus', 'horus', 'eye of ra', 'talk:eye of horus', 'litany of the eye of horus', 'eye of horus (video game)', 'eye of providence', 'osiris myth', 'set (deity)', 'wadjet']
Final Recall level: 9/10
Average precision: 0.9
Normalized DCG for "Eye of Horus": 0.7574884226371842
***********************************************************************************************
Retrieved documents:
['adam ant', 'justin timberlake', 'palindrome', 'classical hollywood cinema', 'list of grange hill characters', 'wikipedia:recent additions 215', 'louis, count of vermandois', 'mary rogers', '1975 in comics', 'what do you want from live']
Relevant documents:
['palindrome', 'madam adam', 'mark saltveit', 'the palindromist', 'young and rich', 'madam secretary (tv series)', 'anda adam', 'dax jordan', 'adam ant', 'what do you want from live']
Final Recall level: 3/10
Average precision: 0.19666666666666666
Normalized DCG for "Madam I'm Adam": 0.289374217567778
***********************************************************************************************
Retrieved documents:
['wikipedia:requests for arbitration/c68-fm-sv/workshop', 'precision and recall', 'accuracy and precision', 'wikipedia:articles for deletion/encyclopedia dramatica (2nd nomination)', 'f-score', "wikipedia:administrators' noticeboard/archive144", 'wikipedia:village pump (policy)/archive 46', 'first amendment to the united states constitution', 'p-value', 'programming language']
Relevant documents:
['mean average precision', 'evaluation measures (information retrieval)', 'precision and recall', 'mean absolute percentage error', 'mean reciprocal rank', 'f-score', 'accuracy and precision', 'information retrieval', 'average absolute deviation', 'symmetric mean absolute percentage error']
Final Recall level: 3/10
Average precision: 0.17666666666666667
Normalized DCG for "Mean Average Precision": 0.2879518565555956
***********************************************************************************************
Retrieved documents:
['list of nobel laureates in physics', 'list of nobel laureates by university affiliation', 'nobel prize', 'nobel prize controversies', 'list of nobel laureates in chemistry', 'list of female nobel laureates', 'list of nobel laureates by country', 'list of nobel laureates', 'ig nobel prize', 'omar m. yaghi']
Relevant documents:
['list of nobel laureates in physics', 'nobel prize in physics', 'list of nobel laureates', 'nobel prize', 'list of nobel laureates by country', 'nobel prize controversies', 'list of nobel laureates in chemistry', 'ig nobel prize', 'list of italian nobel laureates', 'list of female nobel laureates']
Final Recall level: 8/10
Average precision: 0.6671031746031746
Normalized DCG for "Physics Nobel Prizes": 0.6582977201422514
***********************************************************************************************
Retrieved documents:
['wikipedia:wikiquette assistance/archive45', "owner's manual", 'wikipedia:articles for deletion/encyclopedia dramatica (2nd nomination)', 'wikipedia:suspected sock puppets/fnagaton', 'rtfm', 'floss manuals', "wikipedia:new contributors' help page/archive/2008/may", 'maya script', 'wikipedia:village pump (proposals)/archive 26', "wikipedia:administrators' noticeboard/archive144"]
Relevant documents:
['rtfm', "owner's manual", "owner's manual (tv series)", 'user guide', 'autonomous spaceport drone ship', 'the manual', 'floss manuals', 'wikipedia:simplified manual of style', 'manual', 'file:read the hamster manual advertisement.jpg']
Final Recall level: 3/10
Average precision: 0.13999999999999999
Normalized DCG for "Read the manual": 0.4534605839232639
***********************************************************************************************
Retrieved documents:
['foreign involvement in the spanish civil war', 'list of burials at arlington national cemetery', 'united states marine corps', 'background of the spanish civil war', 'russia–spain relations', '1936 in the spanish civil war', 'wikipedia:requests for arbitration/c68-fm-sv/evidence', 'wikipedia:recent additions 215', 'rosario sánchez mora', 'constitution of the united states']
Relevant documents:
['spanish civil war', 'foreign involvement in the spanish civil war', 'republican faction (spanish civil war)', '1936 in the spanish civil war', 'background of the spanish civil war', 'nationalist faction (spanish civil war)', 'german involvement in the spanish civil war', '1938–39 in the spanish civil war', 'spanish coup of july 1936', 'category:spanish civil war']
Final Recall level: 3/10
Average precision: 0.2
Normalized DCG for "Spanish Civil War": 0.4073600285270542
***********************************************************************************************
Retrieved documents:
['roman empire', 'palindrome', 'david slade', 'christmas is coming', 'mageina tovah']
Relevant documents:
['the palindromist', 'palindrome', 'david slade', 'hamsa (bird)', 'geb', 'talk:palindrome', 'greylag goose', 'mageina tovah', 'tengri', 'christmas is coming']
Final Recall level: 4/10
Average precision: 0.27166666666666667
Normalized DCG for "Do geese see god": 0.4809656759585336
***********************************************************************************************
Retrieved documents:
['wikipedia:requests for arbitration/c68-fm-sv/workshop', 'much ado about nothing (1993 film)', "wikipedia:administrators' noticeboard/archive144", 'wikipedia:village pump (policy)/archive 46', 'dogberry', 'much ado', 'much ado about nothing', 'ewan hooper', 'withers building']
Relevant documents:
['much ado about nothing', 'much ado about nothing (1993 film)', 'much ado about nothing (2012 film)', 'don pedro (much ado about nothing)', 'much ado about nothing (opera)', 'beatrice (much ado about nothing)', 'much ado about nothing (disambiguation)', 'much ado', 'template:much ado about nothing', 'much ado about nothing (1973 film)']
Final Recall level: 3/10
Average precision: 0.1261904761904762
Normalized DCG for "Much ado about nothing": 0.4280414671776502
***********************************************************************************************
