import subprocess
import nltk
from nltk import WordNetLemmatizer
from whoosh.index import create_in
from whoosh.fields import *
from whoosh.analysis import Filter, RegexTokenizer, LowercaseFilter, StopFilter, IntraWordFilter, CharsetFilter
import xml.sax
import os
import time
import pickle

from whoosh.support.charset import accent_map

from Code.Normal import base, Indexer


# my_analyzer = None

# Analyzer used for tests
from Code.Normal.base import WordNetLemmatizerFilter

my_analyzer = RegexTokenizer() | LowercaseFilter() | StopFilter() | CharsetFilter(accent_map) | IntraWordFilter() | \
              WordNetLemmatizerFilter()


def get_index():
    # Storing the Tile, ID and Text ("content") of each page
    schema = Schema(title=TEXT(stored=True,
                               field_boost=2,
                               analyzer=my_analyzer, spelling=True),
                    path=ID(stored=True, unique=True),
                    content=TEXT(stored=True,
                                 field_boost=3,
                                 analyzer=my_analyzer))
    # Creates the Index Directory if missing
    if not os.path.exists(base.index_creation_path):
        os.mkdir(base.index_creation_path)
    return create_in(base.index_used_path, schema)


class WikiDocument:
    def __init__(self, title, id, text, ):
        self._title = title
        self._id = id
        self._text = text

    def get_document_title(self):
        return self._title

    def get_document_id(self):
        return self._id

    def get_document_text(self):
        return self._text


class WikiXmlHandler(xml.sax.handler.ContentHandler):
    """Content handler for Wiki XML data using SAX"""

    def __init__(self, titles_to_find):
        xml.sax.handler.ContentHandler.__init__(self)
        self._buffer = None
        self._values = {}
        self._current_tag = None
        self._wiki_documents = []
        self._titles_to_find = titles_to_find
        self._near_last_title = 0
        # Used to retrieve the PageID, the parser would read also the ReviewsIDs
        self._new_page = True

    def characters(self, content):
        """Characters between opening and closing tags"""
        if self._current_tag:
            self._buffer.append(content)

    def startElement(self, name, attrs):
        """Opening tag of element"""
        if name in ('page'):
            # "Unlocks" the ID, because it's a new page
            self._new_page = True
        # Starts saving the ID only if it is the PageID, not a ReviewID
        elif name in ('id') and self._new_page:
            self._current_tag = name
            self._buffer = []
        # Start saving the content, if it's the Title or the Text of the page
        elif name in ('title', 'text'):
            self._current_tag = name
            self._buffer = []

    def endElement(self, name):
        """Closing tag of element"""
        if name == self._current_tag:
            # Saves the buffer only if it containts the Title, the PageID or the Text of the article
            if name not in 'id' or self._new_page:
                self._values[name] = ' '.join(self._buffer)
                # "Locks" the ID of the page, so it will not save the ReviewsIDs
                if name in 'id':
                    self._new_page = True
        # Saves the Title, ID and Text of the page in each of their lists
        if name == 'page':
            # ".lower()" because sometimes the Title of the page can have different
            # uppercased letters than the Title on Google
            current_title = self._values['title']
            if current_title.lower() in self._titles_to_find:
                self._titles_to_find.remove(current_title.lower())

                # Pre-process the title
                # Removing the title with ":" and right after any characters
                # if current_title.find(':') != -1:
                #     if current_title.find(': ') != -1:
                #         self._wiki_documents.append(WikiDocument(current_title, self._values['text']))
                # else:
                self._wiki_documents.append(WikiDocument(current_title, self._values['id'], self._values['text']))

    @property
    def wiki_documents(self):
        return self._wiki_documents


def get_xml_file_num(file):
    return int(file.partition("multistream")[2].partition(".xml")[0])


def get_content_stored():
    if not os.path.exists(base.google_titles_path):
        return []
    with open(base.google_titles_path, 'rb') as fin:
        frame_list = []
        while 1:
            try:
                frame_list.append(pickle.load(fin))
            except EOFError:
                break
        return frame_list


def get_result_titles_stored(content):
    return [query[1] for query in content]


def get_titles_to_find():
    titles_to_find = []
    content = get_content_stored()
    for titles in get_result_titles_stored(content):
        titles_to_find += titles
    # Converting all titles to Lowercase
    return [title.lower() for title in titles_to_find]


def add_documents_to_index(wiki_documents, ix, file):
    # ''' Uncomment to avoid the re-writing of the Index (Debug purposes)
    start_time = time.time()

    print("Start adding documents to the index")
    # Writes the specified fields of every page through the handler
    writer = ix.writer(limitmb=256, procs=4)
    for wikiDocument in wiki_documents:
        writer.add_document(title=wikiDocument.get_document_title(), path=wikiDocument.get_document_id(),
                            content=wikiDocument.get_document_text())
    writer.commit()

    # '''
    f = open(base.used_dumps_path, 'a', encoding='utf-8')
    f.write(file + '\n')
    f.close()
    # '''

    print("Adding the documents to the Index completed!")
    elapsed_time = time.time() - start_time
    print("Documents adding: " + time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))


def get_index_size():
    size = 0
    for dir_path, dir_names, filenames in os.walk(base.index_creation_path):
        for f in filenames:
            fp = os.path.join(dir_path, f)
            if not os.path.islink(fp):
                size += os.path.getsize(fp)
    return size / (1024 * 1024)


def run_analysis(data):
    indexed_docs_num = 0

    start_time_program = time.time()

    print("Using analyzer: \n")
    for element in my_analyzer.items:
        print(f"{str(element).split('(')[0]} + ")

    ix = get_index()
    # Clear the file containting the files used to build the index
    open(base.used_dumps_path, 'w', encoding='utf-8').close()
    # Get all files in the XML Directory
    all_files = [os.path.join(base.xml_path, file) for file in os.listdir(base.xml_path)]
    # Sort the file based on size, CRESC
    # sorted_files = sorted(all_files, key = os.path.getsize)
    # Sort the file in alphanumeric order
    sorted_files = sorted(all_files, key=get_xml_file_num)

    # Removing duplicated titles
    titles_to_find = list(dict.fromkeys(get_titles_to_find()))

    for file in sorted_files[:base.num_dumps_to_index]:
        handler = WikiXmlHandler(titles_to_find)

        if base.dirty_index :
            if os.path.basename(file) == base.dirty_dump :
                print('Dump to be fully indexed ------------'+file)
                wiki_full_docs = Indexer.parse_file(file)
                for document in wiki_full_docs:
                    handler.wiki_documents.append(document)

        start_time = time.time()
        print("START FILE: " + file)

        # Parsing object
        parser = xml.sax.make_parser()
        parser.setContentHandler(handler)
        # Decompressing and parsing of each line of the compressed file, one at a time
        for line in subprocess.Popen(['bzcat'], stdin=open(file), stdout=subprocess.PIPE).stdout:
            parser.feed(line)

        print("END FILE: " + file)
        elapsed_time = time.time() - start_time
        print("Parsing and Pre-Processing: " +time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))

        indexed_docs_num = len(handler.wiki_documents)
        add_documents_to_index(handler.wiki_documents, ix, file)

    print("Google titles not found: " + str(len(titles_to_find)))
    print(titles_to_find)

    print("Parsing and Indexing completed!")
    print(f"Numero di documenti presenti nell'index : {indexed_docs_num}")
    elapsed_time = time.time() - start_time_program
    print("Time: " + str(elapsed_time))
    print(time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))

    print(f"-------INDEX SIZE = {get_index_size()} -------------------------------------------------------------------")
    data['size'].append(get_index_size())

    return data


if __name__ == '__main__':
    main_data = {'name': [], 'size': [], 'map': [], 'ndcg': []}
    run_analysis(main_data)
