import os
import pickle
import time
import requests
#pacchetto google -> pip install google , gli altri 2 sono inutili
from googlesearch import search
from bs4 import BeautifulSoup as bs
from Code.Normal.base import google_titles_path, google_queries


def get_google_file_content():
    if not os.path.exists(google_titles_path):
        return []
    with open(google_titles_path, 'rb') as f:
        results = []
        while True:
            try:
                results.append(pickle.load(f))
            except EOFError:
                break
        return results


def get_google_titles(url):
    page = requests.get(url)
    parsed_page = bs(page.text, 'html.parser')
    print(parsed_page.title.text.split(' -')[0])
    return parsed_page.title.text.split(' -')[0]


def google_titles_to_file(query, titles):
    if not os.path.exists(google_titles_path):
        with open(google_titles_path, 'wb') as file:
            pickle.dump((query, titles), file)
    else:
        with open(google_titles_path, 'ab') as file:
            pickle.dump((query, titles), file)


if __name__ == '__main__':
    file_content = get_google_file_content()
    queries_stored = [query[0] for query in file_content]
    titles_stored = [query[1] for query in file_content]

    for idx, query in enumerate(queries_stored):
        print(f'{query}: {titles_stored[idx]}')

    for idx, google_query in enumerate(google_queries):
        if google_query not in queries_stored:
            if idx != 0:
                print("Pausing the Script.")
                print('----------------------------------------')
                time.sleep(20.5)
            google_titles = [get_google_titles(url) for url in search(google_query + ' site:en.wikipedia.org', lang='en')]

            for title in google_titles:
                if title.find(':') and not title.find(': '):
                    google_titles.remove(title)
            google_titles_to_file(google_query, google_titles[:30])
