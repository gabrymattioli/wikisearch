# -*- coding: utf-8 -*-

import requests
import os
# Library for parsing HTML
from bs4 import BeautifulSoup
# Library for downloading files
from keras.utils import get_file

from Code.Normal import base


def download_files(pathDownloadDirectory, dumps_to_download, data_paths, file_info):
    # Iterate through each file
    for dump in dumps_to_download:
        path = pathDownloadDirectory + dump
        
        # Check to see if the path exists (if the file is already downloaded)
        if not os.path.exists(path):
            # If not, download the file
            data_paths.append(get_file(path, dump_url + dump))
            # Find the file size in MB
            file_size = os.stat(path).st_size / 1e6
            
            # Find the number of articles
            file_articles = int(dump.split('p')[-1].split('.')[-2]) - int(dump.split('p')[-2])
            file_info.append((dump, file_size, file_articles))
            
        # If the file is already downloaded find some information
        else:
            data_paths.append(path)
            # Find the file size in MB
            file_size = os.stat(path).st_size / 1e6
            
            # Find the number of articles
            file_number = int(dump.split('p')[-1].split('.')[-2]) - int(dump.split('p')[-2])
            file_info.append((dump.split('-')[-1], file_size, file_number))
   

if __name__ == "__main__":
    
    # Connecting to the english dump
    base_url = 'https://dumps.wikimedia.org/enwiki/'
    index = requests.get(base_url).text
    soup_index = BeautifulSoup(index, 'html.parser')
    # Find the links on the page
    dumps = [a['href'] for a in soup_index.find_all('a') if 
             a.has_attr('href')]

    # Selecting the dump before the latest dump, that could be in progress
    dump_url = base_url + dumps[len(dumps) - 4]
    # Retrieve the html
    dump_html = requests.get(dump_url).text

    # Convert the selected dump page to a soup
    soup_dump = BeautifulSoup(dump_html, 'html.parser')

    files = []
    # Search through all files
    for file in soup_dump.find_all('li', {'class': 'file'}):
        text = file.text
        # Select the relevant files
        if 'pages-articles-multistream' in text:
            files.append((text.split()[0], text.split()[1:]))

    files_to_download = []        
    indexes_to_download = []
    # Selecting partitioned XML
    for file in files:
        linkFile = file[0]
        if '.xml-p' in linkFile:
            files_to_download.append(linkFile)

    # Download up to 15GB of XML Files
    data_paths_XML = []
    file_info_XML = []
    print('Downloading XML Files')
    download_files(base.xml_path, files_to_download[:base.num_dumps_to_download], data_paths_XML, file_info_XML)
    print("\nInfo files XML: ")
    print(file_info_XML)
