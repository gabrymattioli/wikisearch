import numpy
from whoosh.analysis import LowercaseFilter, StopFilter

import pandas as pd
import os
from Code.Normal import base
import itertools
import matplotlib.pyplot as plt

import IndexerOnlyTitles as indexer
import Evaluation


def indexing_and_evaluation(data, params_combo, dir_name):
    """
    Run indexing and evaluation of a given combo, if respective folder does not exists yet.
    """
    if not os.path.exists(os.path.join(base.evaluation_path, dir_name)):
        data['name'].append(dir_name)
        data = indexer.run_analysis(data)
        data, params_combo[dir_name] = Evaluation.run_evaluation(dir_name, data)
        print(data)
        print(params_combo)

    return data, params_combo


def set_analyzers():
    """
    Iterate over all permutations and set an analyzer each time.
    """
    if not os.path.exists(base.evaluation_path):
        os.mkdir(base.evaluation_path)

    data = {'name': [], 'size': [], 'map': [], 'ndcg': []}
    params_combo = {}

    for tokenizer in base.tokenizers:
        indexer.my_analyzer = tokenizer | LowercaseFilter() | StopFilter()

        dir_name = f"{str(tokenizer).split('(')[0]}+LowercaseFilter+StopFilter"
        data, params_combo = indexing_and_evaluation(data, params_combo, dir_name)

        for counter in range(len(base.filters)):
            for current_filters in itertools.permutations(base.filters, counter + 1):
                current_dir_name = dir_name
                for cf in current_filters:
                    indexer.my_analyzer.items.append(cf)
                    current_dir_name += f"+{str(cf).split('(')[0]}"

                data, params_combo = indexing_and_evaluation(data, params_combo, current_dir_name)

                for cf in current_filters:
                    indexer.my_analyzer.items.remove(cf)

    df = pd.DataFrame(data=data)
    df.to_csv(base.project_path + "/gestione.csv")

    fig1, ax = plt.subplots()
    fig2, (ax1, ax2) = plt.subplots(2, sharex="col")

    recall_each_level = [level / 10 for level in range(0, base.recall_levels)]
    x_array = numpy.array([i for i in range(len(base.google_queries))])
    for combo in data['name']:
        ax.plot(recall_each_level, params_combo[combo]['prec_recall'], label=combo)
        ax.legend(loc='center left', bbox_to_anchor=(1, 1))

        ax1.plot(x_array, params_combo[combo]['avg_precision_each_query'], label=combo)
        ax1.legend(loc='center left', bbox_to_anchor=(1, 1))
        ax2.plot(x_array, params_combo[combo]['ndcg_each_query'], label=combo)

    fig1.savefig(base.evaluation_path + "/overall_prec_recall.png", bbox_inches="tight")
    fig2.savefig(base.evaluation_path + "/overall_avg_ndcg.png", bbox_inches="tight")


if __name__ == '__main__':
    set_analyzers()
