import shutil
from math import log2

from whoosh.index import open_dir
import os
import matplotlib.pyplot as plt
import numpy
import pickle
from nltk.stem.lancaster import LancasterStemmer
from nltk.stem import WordNetLemmatizer
from whoosh.analysis import Filter

from Code.Normal import base, Searcher

lancStemmer = LancasterStemmer()
wnl = WordNetLemmatizer()


class WordNetLemmatizerFilter(Filter):
    """
    Using Word Net Lemmatizer
    """

    def __call__(self, tokens):
        for t in tokens:
            t.text = wnl.lemmatize(t.text)
            yield t


class LancasterStemFilter(Filter):
    """
    Using the NLTK Lancaster Stemmer
    """

    def __call__(self, tokens):
        for t in tokens:
            t.text = lancStemmer.stem(t.text)
            yield t


def average_precision(retrieved_documents, relevant_documents, file):
    """
    Function to compute Average Precision.
    :param retrieved_documents, Documents retrieved by our Search Engine
    :param relevant_documents, Documents relevant for a query
    :param file, File on which save information
    """
    retrieved_doc_count = 0
    relevant_doc_count = 0
    precision = 0
    recall_precision = {0: 0}

    for retrieved_document in retrieved_documents:
        retrieved_doc_count += 1
        if retrieved_document in relevant_documents:
            relevant_doc_count += 1
            precision += float(relevant_doc_count / retrieved_doc_count)
            # adding a new precision value for the new recall level
            recall_precision[(relevant_doc_count / 10)] = (relevant_doc_count / retrieved_doc_count)

    # interpolated precision at 11 standard levels of recall
    for level in range(0, base.recall_levels):
        if list(recall_precision.values())[level:]:
            recall_precision[level / 10] = max(list(recall_precision.values())[level:])
        else:
            recall_precision[level / 10] = 0

    file.write(f'Final Recall level: {relevant_doc_count}/{base.recall_levels - 1}\n')
    return precision / (base.recall_levels - 1) if precision > 0 else 0, recall_precision


def discounted_cumulative_gain(results, optimal_results):
    """
    Function to compute Discounted Cumulative Gain.
    :param results, Set of documents to evaluate
    :param optimal_results, Set of optimal documents to compare with others
    """
    dcg = 0

    for idx, doc in enumerate(results):
        relevance = float(base.ranking_values[optimal_results.index(doc)]) if doc in optimal_results else 0

        if idx == 0:
            dcg += relevance
        else:
            dcg += relevance / log2(idx + 1)

    return dcg


def get_content_stored():
    """
    Function to get a list of all the queries and for each query the first n titles retrieved by google
    """
    if not os.path.exists(base.google_titles_path):
        return []
    with open(base.google_titles_path, 'rb') as fin:
        frame_list = []
        while True:
            try:
                frame_list.append(pickle.load(fin))
            except EOFError:
                break

    return frame_list


def run_evaluation(analyzer_name, data):
    # Create a directory to store evaluation information about the analyzer used
    evaluation_path = os.path.join(base.evaluation_path, analyzer_name)
    if os.path.exists(evaluation_path):
        shutil.rmtree(evaluation_path)
    os.mkdir(evaluation_path)

    os.chmod(evaluation_path, 0o777)

    evaluation_info_file = evaluation_path + '/info.txt'
    evaluation_final_info_file = evaluation_path + '/final_info.txt'

    content_stored = get_content_stored()

    # Splitting content in queries list and titles_retrieved list
    google_queries = [element[0] for element in content_stored]
    google_retrieved_titles = [element[1] for element in content_stored]
    ix = open_dir(base.index_used_path)

    avg_precision_list = []
    list_of_precisions = []
    # Dictionary of a dictionary for storing a list of precision for every lvl of recall for each query
    recall_precision_diary = {}
    params_graphs = {'prec_recall': [], 'avg_precision_each_query': [], 'ndcg_each_query': []}
    normalized_dcg_list = []
    with open(evaluation_info_file, 'w', encoding='utf-8') as f:
        for counter, query in enumerate(google_queries):
            google_results = [
                title.lower() for title in google_retrieved_titles[counter][:base.num_documents_to_evaluate]
            ]

            # index searching
            our_results = []
            if len(google_results) > 0:
                results, _ = Searcher.get_query_results(ix, query)
                our_results = [hit['title'].lower() for hit in results]

            if len(our_results) > 0:
                f.write(f"Retrieved documents:\n{our_results}\n")
                f.write(f"Relevant documents:\n{google_results}\n")

                avp, recall_precision_diary[query] = average_precision(our_results, google_results, f)
                f.write(f"Average precision: {avp}\n")
                standard_dcg = discounted_cumulative_gain(google_results, google_results)
                current_dcg = discounted_cumulative_gain(our_results, google_results)
                normalized_dcg = current_dcg / standard_dcg
                f.write(f"Normalized DCG for \"{query}\": {normalized_dcg}\n")
            else:
                avp, recall_precision_diary = None, {}
                normalized_dcg = None
                f.write('WARNING! Set is empty.\n')

            avg_precision_list.append(avp) if avp is not None else None

            # lista precision ad ogni lvl di recall
            try:
                list_of_precisions.append(recall_precision_diary[query].values())
            except KeyError:
                print(f"Nessun risultato per {query}")


            normalized_dcg_list.append(normalized_dcg) if normalized_dcg is not None else None

            f.write('***********************************************************************************************\n')

    mean_average_precision = sum(avg_precision_list) / len(avg_precision_list) if len(avg_precision_list) > 0 else 0
    mean_normalized_dcg = sum(normalized_dcg_list) / len(normalized_dcg_list) if len(normalized_dcg_list) > 0 else 0
    mean_precision_recall = [numpy.mean(x) for x in zip(*list_of_precisions)]
    with open(evaluation_final_info_file, 'w', encoding='utf-8') as f:
        f.write(f"MAP: {mean_average_precision}\n")
        f.write(f"NDCG: {mean_normalized_dcg}\n")
        f.write(f"Precision averaged over 30 queries for each recall lvl: {mean_precision_recall}\n")
        f.write(f"List of average precision for each query: {avg_precision_list}\n")
        f.write(f"list of ndcg for each query: {normalized_dcg_list}\n")

    data['map'].append(mean_average_precision)
    data['ndcg'].append(mean_normalized_dcg)

    params_graphs['prec_recall'] = mean_precision_recall
    params_graphs['avg_precision_each_query'] = avg_precision_list
    params_graphs['ndcg_each_query'] = normalized_dcg_list

    precision_array = numpy.array(avg_precision_list)
    normalized_dcg_array = numpy.array(normalized_dcg_list)
    x_array = numpy.array([i for i in range(1, len(avg_precision_list) + 1)])

    fig, (ax1, ax2) = plt.subplots(2, sharex="col")

    ax1.set(title='Average Precision', ylabel='PRECISION')
    ax1.bar(x_array, precision_array, color='red')
    ax1.axis([0, 31, 0, 1])

    ax2.set(title='Normalized Discounted Cumulative Gain', xlabel='QUERY', ylabel='NDCG')
    ax2.bar(x_array, normalized_dcg_array, 0.5, color='black')
    ax2.axis([0, 31, 0, 1])
    # plt.show(block=True)
    plt.savefig(evaluation_path + "/avg_precision+ndcg.png")
    plt.close(fig)

    # Decommentare per visualizzare precision ad ogni lvl di recall per ogni query
    # for query in google_queries:
    # precision_each_level = list(recall_precision_diary[query].values())
    # recall_each_level = list(recall_precision_diary[query].keys())
    # recall_array = numpy.array(recall_each_level)
    # precision_array = numpy.array(precision_each_level)
    # fig, ax = plt.subplots()
    # ax.set(title=query)
    # ax.plot(recall_array,single_precision_array)
    # plt.show()
    recall_each_level = [level / 10 for level in range(0, base.recall_levels)]

    fig, ax = plt.subplots()
    ax.set(title="Interpolated precision averaged over 30 queries", xlabel='RECALL', ylabel="PRECISION")
    ax.plot(recall_each_level, mean_precision_recall)
    # plt.show()
    plt.savefig(evaluation_path + "/precision_recall.png")
    plt.close(fig)
    return data, params_graphs


if __name__ == '__main__':
    if not os.path.exists(base.evaluation_path):
        os.mkdir(base.evaluation_path)

    main_data = {'name': [], 'size': [], 'map': [], 'ndcg': []}
    main_data = run_evaluation(base.evaluation_folder, main_data)
    # print(f"DATA: {main_data} ----------------------------------------------------------------------------------------")
    print("Evaluation completed, you can find it in"
          f" {base.evaluation_path.split(base.project_path)[1]}{base.evaluation_folder}")

