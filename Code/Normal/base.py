import os
from os.path import dirname

from nltk import WordNetLemmatizer, LancasterStemmer
# from porter2stemmer import Porter2Stemmer
from whoosh.analysis import RegexTokenizer, StemFilter, IntraWordFilter, CharsetFilter, Filter
from whoosh.support.charset import accent_map

# Commented lines are old tests left for clarity


# lancaster_stemmer = LancasterStemmer()
# porter2 = Porter2Stemmer()

# class Porter2StemFilter(Filter):
#     """
#     Using PorterStemmer2 implementation
#     """
#     def __call__(self, tokens):
#         for t in tokens:
#             t.text = porter2.stem(t.text)
#             yield t
#
# class LancasterStemFilter(Filter):
#     """
#     Using the NLTK Lancaster Stemmer
#     """
#     def __call__(self, tokens):
#         for t in tokens:
#             t.text = lancaster_stemmer.stem(t.text)
#             yield t
#
wnl = WordNetLemmatizer()


class WordNetLemmatizerFilter(Filter):
    """
    Using the NLTK WordNet Lemmatizer
    """
    def __call__(self, tokens):
        for t in tokens:
            t.text = wnl.lemmatize(t.text)
            yield t


project_path = os.path.join(dirname(__file__), '../..')

# sys.path.append(project_path)

xml_path = project_path + '/Xml/'

index_path = project_path + '/Index/'

code_path = project_path + '/Code/'

index_creation_path = index_path + 'MyIndex'

index_used_path = index_path + 'MyIndex'

files_path = project_path + '/Files/'

used_dumps_path = files_path + 'used_dumps.txt'

google_titles_path = files_path + 'google_titles.pickle'

num_dumps_to_index = 10

num_dumps_to_download = 100

num_documents_to_evaluate = 10

dirty_index = True

dirty_dump = 'enwiki-20201220-pages-articles-multistream15.xml-p17324603p17460152.bz2'

evaluation_path = project_path + '/Evaluations_Exam/'

evaluation_folder = 'Results'

recall_levels = 11

ranking_values = [6, 5, 4, 3, 2, 1, 1, 1, 1, 1]

google_queries = [
                'DNA', 'Apple', 'Epigenetics', 'Hollywood', 'Maya', 'Microsoft', 'Precision', 'Tuscany', '99 balloons',
                'Computer Programming', 'Financial meltdown', 'Justin Timberlake', 'Least Squares', 'Mars robots',
                'Page six', 'Roman Empire', 'Solar energy', 'Statistical Significance', 'Steve Jobs', 'The Maya',
                'Triple Cross', 'US Constitution', 'Eye of Horus', 'Madam I\'m Adam', 'Mean Average Precision',
                'Physics Nobel Prizes', 'Read the manual', 'Spanish Civil War', 'Do geese see god',
                'Much ado about nothing',
                ]

filters = [CharsetFilter(accent_map), IntraWordFilter(), WordNetLemmatizerFilter()]

tokenizers = [RegexTokenizer()]

'''
def getProcessedText(text):

    # # Removing numbers
    # text = re.sub(r'\d+', '', text)
    # # Return the text with manual Pre-Processing
    # stemmer = LancasterStemmer()
    # stop_words = set(stopwords.words('english'))
    # puntuaction = str.maketrans(' ', ' ', string.punctuation)
    # wnl = WordNetLemmatizer()
    # return ''.join(stemmer.stem(i) + " " for i in word_tokenize(text.lower().strip()) if not i in stop_words)
    #
    # # Using rake_nltk
    # rake.extract_keywords_from_text(text)
    # #eturn (''.join(stemmer.stem(key) + " " for key in rake.get_ranked_phrases())
    #
    # return (''.join(stemmer.stem(key[0]) + " " for key in rake.run(text, maxWords = 1))

    return None
'''