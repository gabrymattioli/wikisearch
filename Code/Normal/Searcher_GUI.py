import PySimpleGUI as sg
from Code.Normal.Indexer import WordNetLemmatizerFilter
from whoosh.index import open_dir
from Code.Normal import base
from Searcher import get_query_results
from whoosh import highlight


class KeywordFormatter(highlight.Formatter):
    def format_token(self, text, token, replace=False):
        return " @@@%s@@@ " % highlight.get_text(text, token, replace).upper()


def format_results(results):
    results.formatter = KeywordFormatter()
    return ''.join(
        f"{result.pos + 1}- "
        f"{result['title']} \n"
        # dynamic summary if it exists, else first 500 chars
        f" {result.highlights('content') if result.highlights('content') != '' else result['content'][:500]} \n\n "
        for result in results)


if __name__ == "__main__":
    sg.theme('DarkTeal6')  # Add a touch of color
    ix = open_dir(base.index_used_path)
    # All the stuff inside your window.
    layout = [[sg.Text('Welcome to Wiki Searcher. What are you searching for?')],
              [sg.Text('Insert your query'), sg.InputText(key='-QUERY-')],
              [sg.Button('Search', bind_return_key=True), sg.Button('Exit')],

              [sg.Text('Insert your query',size=(0, 1), key="_CORRECTION_"),
               sg.Button('<- Apply Correction', disabled=True, key="_CORRECT_QUERY_")],
              [sg.Multiline(size=(180, 35), font="Helvetica 14", key='_RESULTS_BOX_')]]

    # Create the Window
    window = sg.Window('Wiki Search', layout)

    while True:
        # Event Loop to process "events" and get the "values" of the inputs
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Exit':  # if user closes window or clicks exit
            break

        if event == '_CORRECT_QUERY_':
            window.Element('-QUERY-').update(correction)
            window.Element('_CORRECTION_').update("")
            window.Element('_CORRECT_QUERY_').update(disabled=True)

        if event == 'Search' and values['-QUERY-'] != '':
            # print(values['-QUERY-'])

            window.Element('_RESULTS_BOX_').update("")  # empty from previous search content
            results, correction = get_query_results(ix, values['-QUERY-'])

            if correction != values['-QUERY-']:
                window.Element('_CORRECTION_').update(f"{correction}")
                window.Element('_CORRECT_QUERY_').update(disabled=False)
            else:
                window.Element('_CORRECTION_').update("")
                window.Element('_CORRECT_QUERY_').update(disabled=True)

            for idx, result_text in enumerate(format_results(results).split("@@@")):
                if idx % 2 != 0:
                    # text contains the highlighted keyword
                    window.Element('_RESULTS_BOX_').update(result_text, text_color_for_value="red", append=True)
                else:
                    # plain text
                    window.Element('_RESULTS_BOX_').update(result_text, append=True)

        if event == 'Search' and values['-QUERY-'] == '':
            sg.Popup('No query entered!')
            continue

    window.close()

