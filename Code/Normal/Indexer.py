import subprocess

from nltk import WordNetLemmatizer
from whoosh.analysis import Filter, RegexTokenizer, LowercaseFilter, StopFilter, IntraWordFilter, \
    CharsetFilter
from whoosh.index import create_in
from whoosh.fields import *
import os.path
import time

from whoosh.support.charset import accent_map

from Code.Normal import base
from Code.Normal import Parser

wnl = WordNetLemmatizer()


class WordNetLemmatizerFilter(Filter):
    """
    Using Word Net Lemmatizer
    """

    def __call__(self, tokens):
        for t in tokens:
            t.text = wnl.lemmatize(t.text)
            yield t


my_analyzer = RegexTokenizer() | LowercaseFilter() | StopFilter() | CharsetFilter(accent_map) | IntraWordFilter() | \
              WordNetLemmatizerFilter()


def get_index():
    path = os.getcwd()
    schema = Schema(title=TEXT(stored=True, analyzer=my_analyzer),
                    path=ID(stored=True, unique=True),
                    content=TEXT(stored=True, analyzer=my_analyzer))

    # Creazione cartella di index se non esiste
    if not os.path.exists(base.index_creation_path):
        os.mkdir(base.index_creation_path)
    return create_in(base.index_used_path, schema)


def parse_file(file):
    parser = Parser.getParser()
    handler = parser.getContentHandler()

    start_time = time.time()
    print("START FULL FILE: " + file)
    # Decompressing and parsing of each line of the compressed file, one at a time
    for line in subprocess.Popen(['bzcat'], stdin=open(file), stdout=subprocess.PIPE).stdout:
        parser.feed(line)

    print("END FULL FILE: " + file)

    elapsed_time = time.time() - start_time
    print("Parsing and Pre-Processing full dump: " + str(elapsed_time))
    print(time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))
    wikiDoc = handler.docs
    return wikiDoc


def add_document_to_index(wikiDoc, ix):
    writer = ix.writer(limitmb=256, procs=4)
    for document in wikiDoc:
        writer.add_document(title=document.get_document_title(),
                            path=document.get_document_id(),
                            content=document.get_document_text())
    writer.commit()


def get_num_of_xml_file(file):
    return int(file.partition("multistream")[2].partition(".xml")[0])


if __name__ == "__main__":
    ix = get_index()
    all_files = [os.path.join(base.xml_path, file) for file in os.listdir(base.xml_path)]
    print(all_files)
    sorted_files = sorted(all_files, key=get_num_of_xml_file)

    start_time = time.time()

    for file in sorted_files[:base.num_dumps_to_index]:
        add_document_to_index(parse_file(file), ix)

    f = open(base.used_dumps_path, 'a', encoding='utf-8')

    for file in sorted_files[:base.num_dumps_to_index]:
        f.write(file + '\n')
    f.close()

    print("Total adding of the documents to the Index completed!")
    elapsed_time = time.time() - start_time
    print("Documents adding: " + str(elapsed_time))
    print(time.strftime("%H:%M:%S", time.gmtime(elapsed_time)))
