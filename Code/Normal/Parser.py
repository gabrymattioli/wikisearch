import xml.sax

from Code.Test.IndexerOnlyTitles import WikiDocument


class WikipediaXmlHandler(xml.sax.ContentHandler):
    """
    Manage content of Wikipedia XML data using SAX API.
    """
    def __init__(self):
        self.current_tag = None
        self.buffer = None
        self._values = {}
        self.docs = []

    def startElement(self, tag, attributes):
        """
        Tag is started.
        :param tag: tag started.
        :param attributes: attributes of the started tag.
        """
        self.current_tag = tag
        self.buffer = []
        if tag == "page":
            pass
            # print("*****Document*****")

    def endElement(self, tag):
        """
        A tag is ended.
        :param tag: Ended tag.
        """
        if tag == self.current_tag:
            self._values[tag] = ''.join(self.buffer)

        if tag == 'page':
            current_title = self._values['title']
            self.docs.append(WikiDocument(current_title, self._values['id'], self._values['text']))
            self._values = {}

        self.current_tag = None

    def characters(self, content):
        """
        Read the content of a line.
        :param content: content of the line.
        """
        if self.current_tag:
            self.buffer.append(content)

    def print_prova(self):
        for doc in self.docs:
            print('*****Document*****')
            print(doc['title'])
            print(doc['text'])


def getParser():
    Handler = WikipediaXmlHandler()
    # create an XMLReader
    parser = xml.sax.make_parser()
    parser.setContentHandler(Handler)
    return parser


''''# turn off namepsaces
parser.setFeature(xml.sax.handler.feature_namespaces, 0)
# override the default ContextHandler


parser.parse("wiki_small.xml")
Handler.print_prova()'''

