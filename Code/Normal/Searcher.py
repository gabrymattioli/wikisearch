from whoosh.qparser import MultifieldParser, FuzzyTermPlugin
from whoosh import query as whoosh_query, scoring

from Code.Normal import base


def get_query_results(ix, query):
    parser = MultifieldParser(["title", "content"], schema=ix.schema)
    # Enable FuzzyPlugin for edit-distance searching
    parser.add_plugin(FuzzyTermPlugin())
    parsed_query = parser.parse(query)
    searcher = ix.searcher()

    corrected = searcher.correct_query(parsed_query, query)

    results = searcher.search(parsed_query, limit=base.num_documents_to_evaluate)

    # Extracting first 100 keywords for the first 10 results for query
    keywords = [keyword for keyword, score in results.key_terms("title", docs=base.num_documents_to_evaluate,
                                                                numterms=100)]

    # Expanding the query
    if len(keywords) > 0:
        results2 = searcher.search(whoosh_query.And([whoosh_query.Term("content", t) for t in keywords]),
                                   limit=base.num_documents_to_evaluate)
        results.upgrade(results2)

    return results, corrected.string

