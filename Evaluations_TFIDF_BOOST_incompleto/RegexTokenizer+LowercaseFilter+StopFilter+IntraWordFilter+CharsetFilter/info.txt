Retrieved documents:
['dna', 'nucleic acid double helix', 'z-dna', 'epigenomics', 'epigenetics', 'dna synthesis', 'nuclear dna', 'nucleic acid structure', 'dna (disambiguation)', 'nucleic acid']
Relevant documents:
['dna', 'dna (disambiguation)', 'nucleic acid double helix', 'a-dna', 'dna synthesis', 'z-dna', 'nucleic acid structure', 'genomic dna', 'category:dna', 'nuclear dna']
Final Recall level: 7/10
Average precision: 0.5908730158730158
Normalized DCG for "DNA": 0.7777637459246887
***********************************************************************************************
Retrieved documents:
['apple inc.', 'criticism of apple inc.', 'history of apple inc.', 'list of mergers and acquisitions by apple', 'apple ii series', 'steve wozniak', 'list of apple codenames', 'apple ii', 'next', 'apple (symbolism)']
Relevant documents:
['apple inc.', 'apple', 'apple ii series', 'history of apple inc.', 'apple ii', 'criticism of apple inc.', 'apple (symbolism)', 'list of mergers and acquisitions by apple', 'list of apple codenames', 'apple store']
Final Recall level: 8/10
Average precision: 0.7532142857142856
Normalized DCG for "Apple": 0.7076702363937795
***********************************************************************************************
Retrieved documents:
['epigenetics', 'computational epigenetics', 'epigenomics', 'category:epigenetics', 'dna', 'helmholtz zentrum münchen', 'nuclear dna', 'ralf reski', 'template:gene expression']
Relevant documents:
['epigenetics', 'transgenerational epigenetic inheritance', 'behavioral epigenetics', 'computational epigenetics', 'epigenetic therapy', 'epigenetics in learning and memory', 'epigenetic theories of homosexuality', 'category:epigenetics', 'epigenetics of schizophrenia', 'epigenomics']
Final Recall level: 4/10
Average precision: 0.4
Normalized DCG for "Epigenetics": 0.5763427361910385
***********************************************************************************************
Retrieved documents:
['classical hollywood cinema', 'hollywood sign', 'hollywood, florida', 'national register of historic places listings in los angeles', 'cinema of the united states', 'hollywood (disambiguation)', 'hollywood (british tv series)', 'list of awards and nominations received by justin timberlake', 'justin timberlake', 'jtg daugherty racing']
Relevant documents:
['hollywood', 'hollywood (miniseries)', 'cinema of the united states', 'hollywood, florida', 'hollywood (disambiguation)', 'hollywood sign', 'classical hollywood cinema', 'hollywood (british tv series)', 'hollywood (programming language)', 'list of hollywood-inspired nicknames']
Final Recall level: 6/10
Average precision: 0.549047619047619
Normalized DCG for "Hollywood": 0.383742377826315
***********************************************************************************************
Retrieved documents:
['maya (religion)', 'maya script', 'maya peoples', 'ancient maya art', 'maya calendar', 'maya religion', 'classic maya collapse', 'maya', 'mayan languages', 'yucatec maya language']
Relevant documents:
['maya civilization', 'maya peoples', 'maya (religion)', 'maya', 'autodesk maya', 'maya calendar', 'mayan languages', 'maya script', 'maya architecture', 'maya hawke']
Final Recall level: 6/10
Average precision: 0.5091666666666667
Normalized DCG for "Maya": 0.5632502302047231
***********************************************************************************************
Retrieved documents:
['list of mergers and acquisitions by microsoft', 'microsoft', 'history of microsoft', 'criticism of microsoft', 'microsoft windows', 'list of microsoft software', 'united states v. microsoft corp.', 'microsoft home', 'xbox (console)', 'list of microsoft 365 applications']
Relevant documents:
['microsoft', 'history of microsoft', 'list of mergers and acquisitions by microsoft', 'criticism of microsoft', 'microsoft home', 'united states v. microsoft corp.', 'microsoft 365', 'microsoft windows', 'list of microsoft software', 'microsoft live']
Final Recall level: 8/10
Average precision: 0.8
Normalized DCG for "Microsoft": 0.9383939704462901
***********************************************************************************************
Retrieved documents:
['precision and recall', 'accuracy and precision', 'f-score', 'precision engineering', 'precision', 'objective precision', 'precision (statistics)', 'precision (computer science)', 'honing (metalworking)', 'list of nobel laureates in physics']
Relevant documents:
['precision and recall', 'precision', 'accuracy and precision', 'precision (computer science)', 'precision (statistics)', 'f-score', 'precision', 'confusion matrix', 'precision engineering', 'evaluation measures (information retrieval)']
Final Recall level: 7/10
Average precision: 0.6732142857142857
Normalized DCG for "Precision": 0.7891855693296628
***********************************************************************************************
Retrieved documents:
['grand duchy of tuscany', 'tuscany', 'march of tuscany', 'lucca', 'san gimignano', '2007–08 eccellenza', 'montepulciano', 'category:provinces of tuscany', 'category:tuscany', 'villa garzoni']
Relevant documents:
['tuscany', 'grand duchy of tuscany', 'tuscan wine', 'march of tuscany', 'category:provinces of tuscany', 'category:tuscany', 'san gimignano', 'montepulciano', 'category:cities and towns in tuscany', 'lucca']
Final Recall level: 8/10
Average precision: 0.7621031746031746
Normalized DCG for "Tuscany": 0.86254629457721
***********************************************************************************************
Retrieved documents:
['99 luftballons', '99', '99 luftballons (album)', 'nena', 'subprime mortgage crisis', 'goldfinger (band)', 'red balloon', 'carlo karges', 'jörn-uwe fahrenkrog-petersen', 'list of speed racer episodes']
Relevant documents:
['99 luftballons', '99 luftballons (album)', 'nena', 'talk:99 luftballons', 'nena (band)', 'jörn-uwe fahrenkrog-petersen', 'red balloon', 'carlo karges', 'goldfinger (band)', '99']
Final Recall level: 8/10
Average precision: 0.745436507936508
Normalized DCG for "99 balloons": 0.7706529493509625
***********************************************************************************************
Retrieved documents:
['programming language', 'list of programming languages', 'outline of computer programming', 'c (programming language)', 'computer programming', 'computer program', 'history of apple inc.', 'apple inc.', 'programmer', 'steve wozniak']
Relevant documents:
['computer programming', 'computer program', 'category:computer programming', 'programmer', 'computer programming', 'programming language', 'portal:computer programming', 'outline of computer programming', 'list of programming languages', 'c (programming language)']
Final Recall level: 7/10
Average precision: 0.6777777777777778
Normalized DCG for "Computer Programming": 0.445354969505864
***********************************************************************************************
Retrieved documents:
['subprime mortgage crisis', 'financial crisis', '2008–2011 icelandic financial crisis', '1997 asian financial crisis', 'great recession', 'global financial crisis in september 2008', 'panic of 1907', 'microsoft']
Relevant documents:
['financial crisis', 'financial crisis of 2007–2008', 'great recession', 'list of economic crises', '2008–2011 icelandic financial crisis', 'global financial crisis in september 2008', 'global economic crisis', 'subprime mortgage crisis', 'economic collapse', 'panic of 1907']
Final Recall level: 6/10
Average precision: 0.549047619047619
Normalized DCG for "Financial meltdown": 0.6102884255080335
***********************************************************************************************
Retrieved documents:
['justin timberlake', 'list of awards and nominations received by justin timberlake', 'futuresex/lovesounds', 'justin timberlake discography', 'justified (album)', 'cry me a river (justin timberlake song)', 'sticky & sweet tour', 'kiis-fm jingle ball', 'quentin harris', 'australian idol (season 2)']
Relevant documents:
['justin timberlake', 'justin timberlake discography', 'justin timberlake videography', 'cry me a river (justin timberlake song)', 'justified (album)', 'mirrors (justin timberlake song)', 'list of songs recorded by justin timberlake', 'man of the woods', 'futuresex/lovesounds', 'list of awards and nominations received by justin timberlake']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Justin Timberlake": 0.6913681145527952
***********************************************************************************************
Retrieved documents:
['least squares', 'linear least squares', 'total least squares', 'ordinary least squares', 'non-linear least squares', 'partial least squares regression', 'weighted least squares', 'simple linear regression', 'wikipedia:village pump (technical)/archive 38', 'wikipedia:reference desk/archives/mathematics/2008 may 11']
Relevant documents:
['least squares', 'ordinary least squares', 'linear least squares', 'total least squares', 'simple linear regression', 'non-linear least squares', 'generalized least squares', 'weighted least squares', 'linear regression', 'partial least squares regression']
Final Recall level: 8/10
Average precision: 0.8
Normalized DCG for "Least Squares": 0.9234969344865668
***********************************************************************************************
Retrieved documents:
['exploration of mars', 'opportunity (rover)', 'mars exploration rover', 'rover (space exploration)', 'wikipedia:six degrees of wikipedia', '2009 cannes film festival']
Relevant documents:
['mars rover', 'mars exploration rover', 'list of artificial objects on mars', 'curiosity (rover)', 'exploration of mars', 'mars 2020', 'rover (space exploration)', 'opportunity (rover)', 'comparison of embedded computer systems on board the mars rovers', 'mars landing']
Final Recall level: 4/10
Average precision: 0.4
Normalized DCG for "Mars robots": 0.37857912082601425
***********************************************************************************************
Retrieved documents:
['wikipedia:wikiproject spam/coireports/2008, may 11', 'wikipedia:wikiquette assistance/archive45', "wikipedia:administrators' noticeboard/incidentarchive416", "wikipedia:administrators' noticeboard/incidentarchive417", 'ss kroonland', "wikipedia:administrators' noticeboard/archive144", 'ancient rome', 'new york post', 'wikipedia:templates for deletion/log/2008 may 13', 'wikipedia:files for deletion/2008 may 16']
Relevant documents:
['new york post', 'page 6', 'richard johnson (columnist)', 'claudia cohen', 'jared paul stern', 'dailyfill', 'paula froelich', 'sean delonas', 'james brady (columnist)', 'sixdegrees.com']
Final Recall level: 1/10
Average precision: 0.0125
Normalized DCG for "Page six": 0.11377884364223537
***********************************************************************************************
Retrieved documents:
['roman empire', 'western roman empire', 'ancient rome', 'history of the roman empire', 'fall of the western roman empire', 'roman emperor', 'list of roman emperors', 'wikipedia:wikiproject spam/linkreports/fordham.edu', 'roman empire (disambiguation)', 'veneration of mary in the catholic church']
Relevant documents:
['roman empire', 'history of the roman empire', 'roman emperor', 'list of roman emperors', 'demography of the roman empire', 'fall of the western roman empire', 'western roman empire', 'roman empire (disambiguation)', 'roman empire (tv series)', 'ancient rome']
Final Recall level: 8/10
Average precision: 0.788888888888889
Normalized DCG for "Roman Empire": 0.7676148839200567
***********************************************************************************************
Retrieved documents:
['renewable energy', 'solar power in the united states', 'solar energy', 'solar power by country', 'photovoltaics', 'solar panel', 'feed-in tariffs in australia', 'electricity pricing', 'solar 1', 'apple inc.']
Relevant documents:
['solar energy', 'solar power', 'solar panel', 'outline of solar energy', 'solar power in italy', 'solar power by country', 'concentrated solar power', 'solar power in the united states', 'photovoltaics', 'renewable energy']
Final Recall level: 6/10
Average precision: 0.6
Normalized DCG for "Solar energy": 0.4701151984098188
***********************************************************************************************
Retrieved documents:
['statistical hypothesis testing', 'statistical significance', 'exclusion of the null hypothesis', 'p-value', 'power of a test', 'data dredging', 'type i and type ii errors', 'multiple comparisons problem', 'chi-squared test', 'ordinary least squares']
Relevant documents:
['statistical significance', 'p-value', 'statistical hypothesis testing', 'data dredging', 'power of a test', 'multiple comparisons problem', 'type i and type ii errors', 'exclusion of the null hypothesis', 'binomial test', 'talk:statistical significance']
Final Recall level: 8/10
Average precision: 0.8
Normalized DCG for "Statistical Significance": 0.9012639415462896
***********************************************************************************************
Retrieved documents:
['apple inc.', 'steve wozniak', 'history of apple inc.', 'laurene powell jobs', 'next', 'criticism of apple inc.', 'wikipedia:bot requests/archive 20', 'wikipedia:miscellany for deletion/user:sharkface217/award center 2', 'apple ii', 'microsoft']
Relevant documents:
['steve jobs', 'steve jobs (film)', 'steve jobs (book)', 'steve jobs', 'steve wozniak', 'jobs (film)', 'next', 'laurene powell jobs', 'steve jobs (disambiguation)', 'list of artistic depictions of steve jobs']
Final Recall level: 3/10
Average precision: 0.16
Normalized DCG for "Steve Jobs": 0.15361582199010898
***********************************************************************************************
Retrieved documents:
['maya (religion)', 'maya script', 'maya peoples', 'ancient maya art', 'maya calendar', 'maya religion', 'classic maya collapse', 'maya', 'mayan languages', 'yucatec maya language']
Relevant documents:
['maya civilization', 'maya peoples', 'maya architecture', 'history of the maya civilization', 'maya city', 'chichen itza', 'maya', 'classic maya collapse', 'ancient maya art', 'maya religion']
Final Recall level: 5/10
Average precision: 0.25297619047619047
Normalized DCG for "The Maya": 0.2691462494262518
***********************************************************************************************
Retrieved documents:
['triple product', 'triple cross (1966 film)', 'triple cross', 'three crosses square', 'papal cross', 'eddie chapman', 'the triple cross', 'apple inc.', 'wikipedia:peer review/my happiness (song)/archive1', 'ukraine at the 2008 summer olympics']
Relevant documents:
['triple cross', 'triple cross (1966 film)', 'the triple cross', 'papal cross', 'triple product', 'joe palooka in triple cross', 'three crosses square', 'eddie chapman', 'christian cross variants', 'triple crossed (film)']
Final Recall level: 7/10
Average precision: 0.7
Normalized DCG for "Triple Cross": 0.8185983743688762
***********************************************************************************************
Retrieved documents:
['constitution of the united states', 'history of the united states constitution', 'second amendment to the united states constitution', 'article one of the united states constitution', 'list of amendments to the united states constitution', 'fourteenth amendment to the united states constitution', 'constitution of myanmar', 'article two of the united states constitution', 'first amendment to the united states constitution', '2008 california proposition 8']
Relevant documents:
['constitution of the united states', 'article one of the united states constitution', 'list of amendments to the united states constitution', 'constitution', 'history of the united states constitution', 'united states bill of rights', 'article two of the united states constitution', 'first amendment to the united states constitution', 'fourteenth amendment to the united states constitution', 'second amendment to the united states constitution']
Final Recall level: 8/10
Average precision: 0.7763888888888889
Normalized DCG for "US Constitution": 0.7901535071206723
***********************************************************************************************
Retrieved documents:
['horus', 'osiris myth', 'eye of ra', 'eye of providence', 'eye of horus', 'set (deity)', 'eye of horus (video game)', 'wadjet', 'litany of the eye of horus']
Relevant documents:
['eye of horus', 'horus', 'eye of ra', 'talk:eye of horus', 'litany of the eye of horus', 'eye of horus (video game)', 'eye of providence', 'osiris myth', 'set (deity)', 'wadjet']
Final Recall level: 9/10
Average precision: 0.9
Normalized DCG for "Eye of Horus": 0.7574884226371842
***********************************************************************************************
Retrieved documents:
['palindrome', 'young and rich', 'what do you want from live']
Relevant documents:
['palindrome', 'madam adam', 'mark saltveit', 'the palindromist', 'young and rich', 'madam secretary (tv series)', 'anda adam', 'dax jordan', 'adam ant', 'what do you want from live']
Final Recall level: 3/10
Average precision: 0.3
Normalized DCG for "Madam I'm Adam": 0.491008603459362
***********************************************************************************************
Retrieved documents:
['precision and recall', 'accuracy and precision', 'mean reciprocal rank', 'wikipedia:reference desk/archives/science/2008 may 10', 'statistical hypothesis testing', 'wikipedia:articles for deletion/introduction to genetics', 'wikipedia:reference desk/archives/humanities/2008 may 7', 'mean average precision', 'united states marine corps']
Relevant documents:
['mean average precision', 'evaluation measures (information retrieval)', 'precision and recall', 'mean absolute percentage error', 'mean reciprocal rank', 'f-score', 'accuracy and precision', 'information retrieval', 'average absolute deviation', 'symmetric mean absolute percentage error']
Final Recall level: 4/10
Average precision: 0.35
Normalized DCG for "Mean Average Precision": 0.47001241052866477
***********************************************************************************************
Retrieved documents:
['list of nobel laureates in physics', 'list of nobel laureates by university affiliation', 'list of nobel laureates in chemistry', 'nobel prize', 'nobel prize controversies', 'list of female nobel laureates', 'list of nobel laureates by country', 'list of nobel laureates', 'ig nobel prize', 'wolfgang gentner']
Relevant documents:
['list of nobel laureates in physics', 'nobel prize in physics', 'list of nobel laureates', 'nobel prize', 'list of nobel laureates by country', 'nobel prize controversies', 'list of nobel laureates in chemistry', 'ig nobel prize', 'list of italian nobel laureates', 'list of female nobel laureates']
Final Recall level: 8/10
Average precision: 0.6671031746031746
Normalized DCG for "Physics Nobel Prizes": 0.6434006841825282
***********************************************************************************************
Retrieved documents:
['wikipedia:wikiquette assistance/archive45', 'wikipedia:suspected sock puppets/fnagaton', 'rtfm', "owner's manual", "wikipedia:new contributors' help page/archive/2008/may", "wikipedia:administrators' noticeboard/incidentarchive417", 'maya script', 'wikipedia:village pump (proposals)/archive 26', 'wikipedia:articles for deletion/introduction to genetics', 'the manual']
Relevant documents:
['rtfm', "owner's manual", "owner's manual (tv series)", 'user guide', 'autonomous spaceport drone ship', 'the manual', 'floss manuals', 'wikipedia:simplified manual of style', 'manual', 'file:read the hamster manual advertisement.jpg']
Final Recall level: 3/10
Average precision: 0.11333333333333333
Normalized DCG for "Read the manual": 0.3747083502994545
***********************************************************************************************
Retrieved documents:
['foreign involvement in the spanish civil war', 'list of burials at arlington national cemetery', 'united states marine corps', 'background of the spanish civil war', 'russia–spain relations', '1936 in the spanish civil war', 'rosario sánchez mora', 'wikipedia:recent additions 215', 'constitution of the united states', 'spanish cruiser cristóbal colón']
Relevant documents:
['spanish civil war', 'foreign involvement in the spanish civil war', 'republican faction (spanish civil war)', '1936 in the spanish civil war', 'background of the spanish civil war', 'nationalist faction (spanish civil war)', 'german involvement in the spanish civil war', '1938–39 in the spanish civil war', 'spanish coup of july 1936', 'category:spanish civil war']
Final Recall level: 3/10
Average precision: 0.2
Normalized DCG for "Spanish Civil War": 0.4073600285270542
***********************************************************************************************
Retrieved documents:
['roman empire', 'palindrome', 'christmas is coming', 'mageina tovah', 'david slade']
Relevant documents:
['the palindromist', 'palindrome', 'david slade', 'hamsa (bird)', 'geb', 'talk:palindrome', 'greylag goose', 'mageina tovah', 'tengri', 'christmas is coming']
Final Recall level: 4/10
Average precision: 0.27166666666666667
Normalized DCG for "Do geese see god": 0.4467888104293851
***********************************************************************************************
Retrieved documents:
['wikipedia:requests for arbitration/c68-fm-sv/workshop', 'much ado about nothing (1993 film)', "wikipedia:administrators' noticeboard/archive144", 'wikipedia:village pump (policy)/archive 46', 'dogberry', 'much ado', 'much ado about nothing', 'withers building', 'ewan hooper']
Relevant documents:
['much ado about nothing', 'much ado about nothing (1993 film)', 'much ado about nothing (2012 film)', 'don pedro (much ado about nothing)', 'much ado about nothing (opera)', 'beatrice (much ado about nothing)', 'much ado about nothing (disambiguation)', 'much ado', 'template:much ado about nothing', 'much ado about nothing (1973 film)']
Final Recall level: 3/10
Average precision: 0.1261904761904762
Normalized DCG for "Much ado about nothing": 0.4280414671776502
***********************************************************************************************
